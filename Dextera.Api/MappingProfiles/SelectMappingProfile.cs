﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Dextera.Dal.Models;
using Dextera.Dto.Permissions;
using Dextera.Dto.Common;

namespace Dextera.Api.MappingPropfiles
{
    public class SelectMappingProfile : AutoMapper.Profile
    {
        public SelectMappingProfile()
        {

            CreateMap<Assessment, SelectOptionDTO<int>>();
            CreateMap<Attorney, SelectOptionDTO<int>>();
            CreateMap<Group, SelectOptionDTO<int>>();
            CreateMap<Client, SelectOptionDTO<int>>()
                .ForMember(x => x.Name, y => y.MapFrom(z => z.ClientName));


        }
    }
}
