﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Dextera.Dal.Models;
using Dextera.Dto.Permissions;

namespace Dextera.Api.MappingPropfiles
{
    public class PermissionMappingProfile : AutoMapper.Profile
    {
        public PermissionMappingProfile()
        {   
                CreateMap<Permission, PermissionsDTO>()
                .ForMember(m => m.Checked, opt => opt.Ignore())
                .ForMember(m => m.AccessLevel, opt => opt.Ignore())
                
                .ReverseMap();
        }
    }
}
