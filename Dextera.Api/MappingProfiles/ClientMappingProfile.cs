﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Dextera.Dal.Models;
using Dextera.Dto.Permissions;
using Dextera.Dto.Workflow;

namespace Dextera.Api.MappingPropfiles
{
    public class ClientMappingProfile : AutoMapper.Profile
    {
        public ClientMappingProfile()
        {


            CreateMap<Client, ClientDTO>()
                //.ForMember(x => x.)
                
                //.ForMember(x => x.SubClientName, y => y.Ignore())
                //.ForMember(x => x.SubClientType, y => y.Ignore())
                

                .ReverseMap();
                

        }
    }
}
