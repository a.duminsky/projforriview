﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Dextera.Dal.Models;
using Dextera.Dto.Permissions;
using Dextera.Dto.Workflow;

namespace Dextera.Api.MappingPropfiles
{
    public class CaseMappingProfile : AutoMapper.Profile
    {
        public CaseMappingProfile()
        {


            CreateMap<Case, CaseDTO>()
                .ReverseMap();

            CreateMap<Defenant, DefenantDTO>().ReverseMap();
                

        }
    }
}
