﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Dextera.Dal.Models;
using Dextera.Dto.Permissions;

namespace Dextera.Api.MappingPropfiles
{
    public class FirmMappingProfile : AutoMapper.Profile
    {
        public FirmMappingProfile()
        {
            
                CreateMap<Tenant, TenantDTO>().ReverseMap();
            
        }
    }
}
