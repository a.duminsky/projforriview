﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Dextera.Dal.Models;
using Dextera.Dto.Permissions;

namespace Dextera.Api.MappingPropfiles
{
    public class AccountMappingProfile : AutoMapper.Profile
    {
        public AccountMappingProfile()
        {
            CreateMap<ApplicationUser, AccountDTO>()
            .ForMember(x => x.Password, y => y.Ignore())
            .ForMember(x => x.Groups, y => y.MapFrom(z => z.GroupUsers != null ? z.GroupUsers.Select(q => q.GroupId).ToList() : new List<int>()))
            .ForMember(x => x.Roles, y => y.MapFrom(z => z.RoleUser != null ? z.RoleUser.Select(q => q.RoleId).ToList() : new List<int>()))
            .ReverseMap()
            .ForMember(x => x.Roles, y => y.Ignore())
            .ForMember(x => x.PasswordHash, y => y.Ignore())
            .ForMember(x => x.SNNEncrypted, y => y.Ignore())
            .ForMember(x => x.GroupUsers, y => y.Ignore())
            .ForMember(x => x.RoleUser, y => y.Ignore());
        }
    }
}

