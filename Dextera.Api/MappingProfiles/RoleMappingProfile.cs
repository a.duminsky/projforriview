﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Dextera.Dal.Models;
using Dextera.Dto.Permissions;

namespace Dextera.Api.MappingPropfiles
{
    public class RoleMappingProfile : AutoMapper.Profile
    {
        public RoleMappingProfile()
        {
            
            CreateMap<Role, RoleDTO>().ReverseMap();
            CreateMap<Role, RolePermissionsDTO>()
                .ForMember(x => x.Permissions, y => y.Ignore())
                .ReverseMap()
                .ForMember(x => x.RoleUser, y => y.Ignore())
                .ForMember(x => x.Permissions, y => y.Ignore());

        }
    }
}
