﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Dextera.Dal.Models;
using Dextera.Dto.Permissions;
//using Dextera.Api.Models;

namespace Dextera.Api.MappingPropfiles
{
    public class GroupMappingProfile : AutoMapper.Profile
    {
        public GroupMappingProfile()
        {
            CreateMap<Group, GroupDTO>()
            .ForMember(x => x.Roles, y => y.Ignore())
            .ForMember(x => x.Roles, y => y.MapFrom(z => z.GroupRole != null ? z.GroupRole.Select(q => q.RoleId).ToList() : new List<int>()))
            .ReverseMap()
           .ForMember(x => x.GroupRole, y => y.Ignore());

        }
    }
}
