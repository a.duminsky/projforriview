using Dextera.Dal;
using Dextera.Dal.Repository;
using Dextera.Services.Implementations;
using Dextera.Services.Interfaces;
using Microsoft.Practices.Unity;
using System.Web.Http;
using Unity.WebApi;

namespace Dextera.Api
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterType<ITestService, TestService>();
            container.RegisterType<IAccountService, AccountService>();
            container.RegisterType<IPermissionService, PermissionService>();
            container.RegisterType<IRoleService, RoleService>();
            container.RegisterType<IFirmService, FirmService>();
            container.RegisterType<IGroupGridService, GroupGridService>();
            container.RegisterType<IClientService, ClentService>();
            container.RegisterType<ICaseService, CaseService>();
            container.RegisterType<ISelectOptionsService, SelectOptionsService>();
            
            container.RegisterType<IMultitenancyService, MultitenancyService>(new PerThreadLifetimeManager());
            container.RegisterType(typeof(IRepository<>),
                       typeof(Repository<>));
            container.RegisterType(typeof(ITenantRepository<>),
                       typeof(TenantRepository<>));
            container.RegisterType(typeof(DexteraDbContext), typeof(DexteraDbContext));


            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}