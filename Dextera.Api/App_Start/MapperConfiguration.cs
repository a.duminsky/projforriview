﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper.Configuration;
using System.Reflection;
using AutoMapper;

namespace Dextera.Api.App_Start
{
    public class MapperConfiguration
    {

        public static void Initialize()
        {
            AutoMapper.Mapper.Initialize(cfg =>
            {
                var profiles = Assembly.GetExecutingAssembly().GetTypes().Where(x => typeof(Profile).IsAssignableFrom(x));
                foreach (var profile in profiles)
                {
                    cfg.AddProfile(Activator.CreateInstance(profile) as Profile);
                    
                }
                //cfg.AddProfile<AccountMappingProfile>();
                //cfg.AddProfile<RoleMappingProfile>();
                //cfg.AddProfile<FirmMappingProfile>();
                //AutoMapper.Mapper.AssertConfigurationIsValid();
            });
            Mapper.AssertConfigurationIsValid();

        }

        //private static void GetConfiguration(IConfiguration configuration)
        //{
        //    var profiles = Assembly.GetExecutingAssembly().GetTypes().Where(x => typeof(Profile).IsAssignableFrom(x));
        //    foreach (var profile in profiles)
        //    {
        //        configuration.AddProfile(Activator.CreateInstance(profile) as Profile);
        //    }
        //}

        //public static void Configure()
        //{
        //    Mapper.Initialize(x => GetConfiguration(Mapper.Configuration));
        //}
    }
}