﻿using Dextera.Api.Filters;
using Dextera.Dal.Repository;
using Dextera.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dextera.Dto.Permissions;
using Dextera.Dal.Models;
using AutoMapper;
using Newtonsoft.Json;
using Dextera.Services.Kendo;
using Dextera.Dto.Workflow;

namespace Dextera.Api.Controllers
{
    //[System.Web.Http.Cors.EnableCors(origins: "*", headers: "*", methods: "*")]
    [System.Web.Http.RoutePrefix("api/client")]
    [Authorize]
    public class ClientController : BaseApiController
    {
        public IClientService _clientService;

        public IMultitenancyService _multitenancyService;

        public ClientController(IClientService clientService, IMultitenancyService multitenancyService)
        {
            _clientService = clientService;
            _multitenancyService = multitenancyService;
        }
        
        [HttpGet]
        public DataSourceResult GetList(HttpRequestMessage requestMessage)
        {
            var request = JsonConvert.DeserializeObject<DataSourceRequest>(
                   requestMessage.RequestUri.ParseQueryString().GetKey(0));
            return _clientService.GetList(request);
        }


        [HttpGet]
        public ClientDTO Get(int id)
        {
            return _clientService.Get(id);
            
        }
        
        [ValidationFilter]
        [HttpPost]
        public void Update(ClientDTO client)
        {
            _clientService.Update(client);
        }

        [ValidationFilter]
        [HttpPut]
        public void Add(ClientDTO client)
        {
            _clientService.Add(client);
        }

        [HttpDelete]
        public void Delete(ClientDTO client)
        {
            _clientService.Remove(client);
        }
    }
}
