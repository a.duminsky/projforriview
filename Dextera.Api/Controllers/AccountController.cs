﻿using Dextera.Api.Filters;
using Dextera.Dal.Repository;
using Dextera.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dextera.Dto.Permissions;
using Dextera.Dal.Models;
using Newtonsoft.Json;
using Dextera.Services.Kendo;


namespace Dextera.Api.Controllers
{
    
    [System.Web.Http.RoutePrefix("api/account")]
    [MultitenancyFilter]
    public class AccountController : BaseApiController
    {
        public IAccountService _accountService;

        public IMultitenancyService _multitenancyService;

        public AccountController(IAccountService accountService, IMultitenancyService multitenancyService)
        {
            _accountService = accountService;
            _multitenancyService = multitenancyService;
        }
        
        [Authorize]
        [HttpGet]
        public DataSourceResult GetList(HttpRequestMessage requestMessage)
        {
            var request = JsonConvert.DeserializeObject<DataSourceRequest>(
                 requestMessage.RequestUri.ParseQueryString().GetKey(0));
            return _accountService.GetList(request);
           // return _accountService.GetList().Select(x => AutoMapper.Mapper.Map<AccountDTO>(x)).ToList();
        }


        [HttpGet]
        public AccountDTO Get(string id)
        {
            return _accountService.Get(id);
        }

        [HttpPost]
        public void Update(AccountDTO account)
        {
            _accountService.Update(account);
        }

        [HttpDelete]
        public void Delete(AccountDTO account)
        {
            _accountService.Delete(account);
        }

        [HttpPut]
        public void Add(AccountDTO account)
        {
            _accountService.Add(account);
        }


    }
}
