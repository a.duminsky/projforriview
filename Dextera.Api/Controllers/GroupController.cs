﻿using Dextera.Api.Filters;
using Dextera.Dal.Repository;
using Dextera.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dextera.Dto.Permissions;
using Dextera.Dal.Models;
using Dextera.Services.Kendo;
using Newtonsoft.Json;

namespace Dextera.Api.Controllers
{
    //[System.Web.Http.Cors.EnableCors(origins: "*", headers: "*", methods: "*")]
    [System.Web.Http.RoutePrefix("api/group")]
    [MultitenancyFilter]
    [Authorize]
    public class GroupController : BaseApiController
    {
        public IGroupGridService _groupService;
        public IMultitenancyService _multitenancyService;

        public GroupController(IGroupGridService groupService, IMultitenancyService multitenancyService)
        {
            _groupService = groupService;
            _multitenancyService = multitenancyService;
        }

       

        [HttpGet]
        public DataSourceResult GetList(HttpRequestMessage requestMessage)
        {
            var request = JsonConvert.DeserializeObject<DataSourceRequest>(
                   requestMessage.RequestUri.ParseQueryString().GetKey(0));
            return _groupService.GetList(request);
        }


        [HttpGet]
        public GroupDTO Get(int id)
        {
            return AutoMapper.Mapper.Map<GroupDTO>(_groupService.Get(id));
        }

         
        [HttpPost]
        public void Update(GroupDTO group)
        {
            _groupService.Update(group);
        }

        [HttpPut]
        public void Add(GroupDTO group)
        {
         
            _groupService.Add(group);
        }



        //[ValidationFilter]
        //[HttpPut]
        //public void Add(GroupDTO group)
        //{
        //    _groupServicee.Add(group);
        //}

        //[HttpDelete]
        //public void Delete(GroupDTO group)
        //{
        //    _groupServicee.Remove(group);
        //}
    }
}
