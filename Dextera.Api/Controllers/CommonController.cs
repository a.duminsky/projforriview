﻿using Dextera.Api.Filters;
using Dextera.Dal.Repository;
using Dextera.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dextera.Dto.Permissions;
using Dextera.Dal.Models;
using Newtonsoft.Json;
using Dextera.Services.Kendo;
using Dextera.Common.Enums;

namespace Dextera.Api.Controllers
{
    
    [System.Web.Http.RoutePrefix("api/common")]
    [MultitenancyFilter]
    public class CommonController : BaseApiController
    {
        public ISelectOptionsService _selectOptionsService;

        

        public CommonController(ISelectOptionsService selectOptionsService)
        {
            _selectOptionsService = selectOptionsService;
        }
        
        [Authorize]
        [HttpGet]
        [Route("{type}")]
        public DataSourceResult SelectOptions(SelectType type, HttpRequestMessage requestMessage)
        {
            var request = JsonConvert.DeserializeObject<DataSourceRequest>(
                   requestMessage.RequestUri.ParseQueryString().GetKey(0));
            return _selectOptionsService.GetOptions(type, request);
        }
    }
}
