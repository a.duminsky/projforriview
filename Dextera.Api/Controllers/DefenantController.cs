﻿using Dextera.Api.Filters;
using Dextera.Dal.Repository;
using Dextera.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dextera.Dto.Permissions;
using Dextera.Dal.Models;
using AutoMapper;
using Newtonsoft.Json;
using Dextera.Services.Kendo;
using Dextera.Dto.Workflow;

namespace Dextera.Api.Controllers
{
    //[System.Web.Http.Cors.EnableCors(origins: "*", headers: "*", methods: "*")]
    [System.Web.Http.RoutePrefix("api/defenant")]
    [Authorize]
    public class DefenantController : BaseApiController
    {
        public IDefenantService _defenantService;

        public IMultitenancyService _multitenancyService;

        public DefenantController(IDefenantService defenantService, IMultitenancyService multitenancyService)
        {
            _defenantService = defenantService;
            _multitenancyService = multitenancyService;
        }
        
        [HttpGet]
        [Route("{caseId}")]
        public DataSourceResult GetList(int caseId, HttpRequestMessage requestMessage)
        {
            var request = JsonConvert.DeserializeObject<DataSourceRequest>(
                   requestMessage.RequestUri.ParseQueryString().GetKey(0));
            return _defenantService.GetList(request);
        }


        [HttpGet]
        public DefenantDTO Get(int id)
        {
            return _defenantService.Get(id);
            
        }
        
        [ValidationFilter]
        [HttpPost]
        public void Update(DefenantDTO entity)
        {
            _defenantService.Update(entity);
        }

        [ValidationFilter]
        [HttpPut]
        public void Add(DefenantDTO entity)
        {
            _defenantService.Add(entity);
        }

        [HttpDelete]
        public void Delete(DefenantDTO entity)
        {
            _defenantService.Remove(entity);
        }
    }
}
