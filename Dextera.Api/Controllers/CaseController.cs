﻿using Dextera.Api.Filters;
using Dextera.Dal.Repository;
using Dextera.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dextera.Dto.Permissions;
using Dextera.Dal.Models;
using AutoMapper;
using Newtonsoft.Json;
using Dextera.Services.Kendo;
using Dextera.Dto.Workflow;

namespace Dextera.Api.Controllers
{
    //[System.Web.Http.Cors.EnableCors(origins: "*", headers: "*", methods: "*")]
    [System.Web.Http.RoutePrefix("api/case")]
    [Authorize]
    public class CaseController : BaseApiController
    {
        public ICaseService _caseService;

        public IMultitenancyService _multitenancyService;

        public CaseController(ICaseService caseService, IMultitenancyService multitenancyService)
        {
            _caseService = caseService;
            _multitenancyService = multitenancyService;
        }
        
        [HttpGet]
        public DataSourceResult GetList(HttpRequestMessage requestMessage)
        {
            var request = JsonConvert.DeserializeObject<DataSourceRequest>(
                   requestMessage.RequestUri.ParseQueryString().GetKey(0));
            return _caseService.GetList(request);
        }


        [HttpGet]
        public CaseDTO Get(int id)
        {
            return _caseService.Get(id);
            
        }
        
        [ValidationFilter]
        [HttpPost]
        public void Update(CaseDTO entity)
        {
            _caseService.Update(entity);
        }

        [ValidationFilter]
        [HttpPut]
        public void Add(CaseDTO entity)
        {
            _caseService.Add(entity);
        }

        [HttpDelete]
        public void Delete(CaseDTO entity)
        {
            _caseService.Remove(entity);
        }
    }
}
