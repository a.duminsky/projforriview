﻿using Dextera.Api.Filters;
using Dextera.Dal.Repository;
using Dextera.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Dextera.Api.Controllers
{
    [MultitenancyFilter]
    public class ValuesController : BaseApiController
    {
        public ITestService _testService;

        public IMultitenancyService _multitenancyService;

        public ValuesController(ITestService testService, IMultitenancyService multitenancyService)
        {
            _testService = testService;
            _multitenancyService = multitenancyService;
        }
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return _testService.GetValue();
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
