﻿using Dextera.Api.Filters;
using Dextera.Dal.Repository;
using Dextera.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dextera.Dto.Permissions;
using Dextera.Dal.Models;
using Dextera.Services.Kendo;
using Newtonsoft.Json;

namespace Dextera.Api.Controllers
{
    //[System.Web.Http.Cors.EnableCors(origins: "*", headers: "*", methods: "*")]
    [System.Web.Http.RoutePrefix("api/role")]
    [MultitenancyFilter]
    [Authorize]
    public class RoleController : BaseApiController
    {
        public IRoleService _roleService;
        public IMultitenancyService _multitenancyService;

        public RoleController(IRoleService roleService, IMultitenancyService multitenancyService)
        {
            _roleService = roleService;
            _multitenancyService = multitenancyService;
        }
        
        

        [HttpGet]
        public DataSourceResult GetList(HttpRequestMessage requestMessage)
        {
            var request = JsonConvert.DeserializeObject<DataSourceRequest>(
                   requestMessage.RequestUri.ParseQueryString().GetKey(0));
            return _roleService.GetList(request);
        }


        [HttpGet]
        public RolePermissionsDTO Get(int id)
        {
            return _roleService.Get(id);
        }

        [ValidationFilter]
        [HttpPost]
        public void Update(RolePermissionsDTO role)
        {
             _roleService.Update(role);
        }

        [ValidationFilter]
        [HttpPut]
        public void Add(RolePermissionsDTO account)
        {
            _roleService.Add(account);
            
        }


        [HttpDelete]
        public void Delete(RolePermissionsDTO account)
        {
            _roleService.Remove(account);
        }
    }
}
