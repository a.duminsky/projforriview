﻿using Dextera.Api.Filters;
using Dextera.Dal.Repository;
using Dextera.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dextera.Dto.Permissions;
using Dextera.Dal.Models;
namespace Dextera.Api.Controllers
{
    //[System.Web.Http.Cors.EnableCors(origins: "*", headers: "*", methods: "*")]
    [System.Web.Http.RoutePrefix("api/permission")]
    [MultitenancyFilter]
    [Authorize]
    public class PermissionController : BaseApiController
    {
        public IPermissionService _permissionService;

        public IMultitenancyService _multitenancyService;

        public PermissionController(IPermissionService permissionService, IMultitenancyService multitenancyService)
        {
            _permissionService = permissionService;
            _multitenancyService = multitenancyService;
        }
        
        [HttpGet]
        public List<PermissionsDTO> GetList()
        {
            return _permissionService.GetList().Select(x => AutoMapper.Mapper.Map<PermissionsDTO>(x)).ToList();
        }


        [HttpGet]
        public PermissionsDTO Get(int id)
        {
            return AutoMapper.Mapper.Map<PermissionsDTO>(_permissionService.Get(id));
        }

        [HttpPost]
        public void Update(PermissionsDTO permission)
        {
            _permissionService.Update(AutoMapper.Mapper.Map<Permission>(permission));
        }

        [HttpPut]
        public void Add(PermissionsDTO account)
        {
            _permissionService.Add(AutoMapper.Mapper.Map<Permission>(account));
        }

        [HttpDelete]
        public void Delete(PermissionsDTO account)
        {
            _permissionService.Remove(AutoMapper.Mapper.Map<Permission>(account));
        }
    }
}
