﻿using Dextera.Api.Filters;
using Dextera.Dal.Repository;
using Dextera.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dextera.Dto.Permissions;
using Dextera.Dal.Models;
using AutoMapper;
using Newtonsoft.Json;
using Dextera.Services.Kendo;

namespace Dextera.Api.Controllers
{
    //[System.Web.Http.Cors.EnableCors(origins: "*", headers: "*", methods: "*")]
    [System.Web.Http.RoutePrefix("api/firm")]
    [Authorize]
    public class FirmController : BaseApiController
    {
        public IFirmService _firmService;

        public IMultitenancyService _multitenancyService;

        public FirmController(IFirmService firmService, IMultitenancyService multitenancyService)
        {
            _firmService = firmService;
            _multitenancyService = multitenancyService;
        }
        
        [HttpGet]
        public DataSourceResult GetList(HttpRequestMessage requestMessage)
        {
            var request = JsonConvert.DeserializeObject<DataSourceRequest>(
                   requestMessage.RequestUri.ParseQueryString().GetKey(0));
            return _firmService.GetList(request);
        }


        [HttpGet]
        public TenantDTO Get(int id)
        {
            return AutoMapper.Mapper.Map<TenantDTO>(_firmService.Get(id));
            
        }
        
        [ValidationFilter]
        [HttpPost]
        public void Update(TenantDTO role)
        {
            _firmService.Update(role);
        }

        [ValidationFilter]
        [HttpPut]
        public void Add(TenantDTO account)
        {
            _firmService.Add(account);
        }

        [HttpDelete]
        public void Delete(TenantDTO account)
        {
            _firmService.Remove(account);
        }
    }
}
