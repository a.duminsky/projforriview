﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security.OAuth;
using System.Web.Http;

[assembly: OwinStartup(typeof(Dextera.Api.Startup))]

namespace Dextera.Api
{

    public partial class Startup
    {
        //static Startup()
        //{
        //    AuthServerOptions = new OAuthAuthorizationServerOptions
        //    {
        //        AllowInsecureHttp = true,
        //        TokenEndpointPath = new PathString("/token"),
        //        AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(30),
        //        Provider = new OAuthAuthorizationServerProvider()

        //        // RefreshTokenProvider = new SimpleRefreshTokenProvider()
        //    };
        //}

        public static OAuthAuthorizationServerOptions AuthServerOptions { get; set; }

        public void Configuration(IAppBuilder app)
        {
            this.ConfigureAuth(app);
            HttpConfiguration config = new HttpConfiguration();
            
            //WebApiConfig.Register(config);
            //app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            //app.UseWebApi(config);
            ////AutomapperConfig.Configure();

            app.UseCors(CorsOptions.AllowAll);

        }


    }

}
