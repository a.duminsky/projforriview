﻿using Dextera.Dal.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace Dextera.Api.Filters
{
    public class MultitenancyFilter : System.Web.Http.Filters.ActionFilterAttribute
    {
        public IMultitenancyService Multitenancy { get {
                return (IMultitenancyService)GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(IMultitenancyService));
            }
        }
        public MultitenancyFilter()
        {
            
        }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            
            if (HttpContext.Current.User != null && HttpContext.Current.User.Identity != null)
            {
                Multitenancy.SetUser(HttpContext.Current.User.Identity.Name);
            }
            else
            {
                Multitenancy.SetUser(null);
            }

        }

        
    }
}