﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dextera.Dal.Models;
using Dextera.Services.Kendo;
using Dextera.Dto.Permissions;
using Dextera.Dto.Workflow;

namespace Dextera.Services.Interfaces
{
    public interface ICaseService
    {
        DataSourceResult GetList(DataSourceRequest request);

        CaseDTO Get(int id);

        void Add(CaseDTO role);
        void Update(CaseDTO role);

        void Remove(CaseDTO role);
    }
}
