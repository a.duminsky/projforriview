﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dextera.Dal.Models;
using Dextera.Services.Kendo;
using Dextera.Dto.Permissions;

namespace Dextera.Services.Interfaces
{
    public interface IFirmService
    {
        DataSourceResult GetList(DataSourceRequest request);

        Tenant Get(int id);

        void Add(TenantDTO role);
        void Update(TenantDTO role);

        void Remove(TenantDTO role);
    }
}
