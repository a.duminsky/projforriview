﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dextera.Dal.Models;
using Dextera.Services.Kendo;
using Dextera.Dto.Permissions;
using Dextera.Common.Enums;

namespace Dextera.Services.Interfaces
{
    public interface ISelectOptionsService
    {
        DataSourceResult GetOptions(SelectType type, DataSourceRequest request);


    }
}
