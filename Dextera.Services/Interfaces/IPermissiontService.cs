﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dextera.Dal.Models;
using Dextera.Dto.Permissions;

namespace Dextera.Services.Interfaces
{
    public interface IPermissionService
    {
        List<Permission> GetList();

        Permission Get(int id);

        void Add(Permission permission);
        void Update(Permission permission);

        void Remove(Permission account);

        void UpdatePermissionsForRole(int roleId, List<PermissionsDTO> permissions);
    }
}
