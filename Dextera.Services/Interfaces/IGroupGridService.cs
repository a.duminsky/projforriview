﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dextera.Dto.Permissions;
using Dextera.Dal.Models;
using Dextera.Services.Kendo;

namespace Dextera.Services.Interfaces
{
    public interface IGroupGridService
    {
        DataSourceResult GetList(DataSourceRequest request);
        Group Get(int id);
        void Update(GroupDTO group);
        void Add(GroupDTO group);
        //void Remove(GroupGridDTO group);
    }
}
