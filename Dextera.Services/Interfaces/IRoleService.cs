﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dextera.Dal.Models;
using Dextera.Services.Kendo;
using Dextera.Dto.Permissions;

namespace Dextera.Services.Interfaces
{
    public interface IRoleService
    {
        DataSourceResult GetList(DataSourceRequest request);

        RolePermissionsDTO Get(int id);

        void Add(RolePermissionsDTO role);
        void Update(RolePermissionsDTO role);

        void Remove(RolePermissionsDTO role);
    }
}
