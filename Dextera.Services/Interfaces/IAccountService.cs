﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dextera.Dal.Models;
using Dextera.Services.Kendo;
using Dextera.Dto.Permissions;

namespace Dextera.Services.Interfaces
{
    public interface IAccountService
    {
        DataSourceResult GetList(DataSourceRequest request);

        AccountDTO Get(string id);
        void Update(AccountDTO account);
      
        void Delete(AccountDTO account);
        void Add(AccountDTO account);

    }
}
