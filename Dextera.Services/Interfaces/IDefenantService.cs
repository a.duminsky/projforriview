﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dextera.Dal.Models;
using Dextera.Services.Kendo;
using Dextera.Dto.Permissions;
using Dextera.Dto.Workflow;

namespace Dextera.Services.Interfaces
{
    public interface IDefenantService
    {
        DataSourceResult GetList(DataSourceRequest request);

        DefenantDTO Get(int id);

        void Add(DefenantDTO role);
        void Update(DefenantDTO role);

        void Remove(DefenantDTO role);
    }
}
