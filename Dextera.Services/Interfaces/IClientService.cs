﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dextera.Dal.Models;
using Dextera.Services.Kendo;
using Dextera.Dto.Permissions;
using Dextera.Dto.Workflow;

namespace Dextera.Services.Interfaces
{
    public interface IClientService
    {
        DataSourceResult GetList(DataSourceRequest request);

        ClientDTO Get(int id);

        void Add(ClientDTO client);
        void Update(ClientDTO client);

        void Remove(ClientDTO client);
    }
}
