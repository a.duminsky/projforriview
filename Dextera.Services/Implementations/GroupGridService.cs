﻿using Dextera.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dextera.Dal.Models;
using Dextera.Dal.Repository;
using Dextera.Dto.Permissions;
using Dextera.Services.Kendo;
using AutoMapper;

namespace Dextera.Services.Implementations
{
    public class GroupGridService : IGroupGridService
    {
        
        public ITenantRepository<Group> _groupRepository;
        public ITenantRepository<GroupRole> _roleRepository;

        public GroupGridService(ITenantRepository<Group> groupRepository, ITenantRepository<GroupRole> roleRepository)
        {

            _groupRepository = groupRepository;
            _roleRepository = roleRepository;


        }

        public DataSourceResult GetList(DataSourceRequest request)
        {
            return _groupRepository.Items
                .ToDataSourceResult<Group, GroupDTO>(request.Take, request.Skip, request.Sort, request.Filter);
        }

        public Group Get(int id)
        {
            return _groupRepository.GetById(id);
        }

       
        public void Add(GroupDTO group)
        {
            var groupId = _groupRepository.Add(Mapper.Map<Group>(group));
            //var user = Mapper.Map<Group>(group);
            UpdateRoleForGroup(groupId, group.Roles);
        }

        public void Update(GroupDTO group)
        {
            var dbObj = _groupRepository.GetById(group.Id);
            Mapper.Map<GroupDTO, Group>(group, dbObj);
            _groupRepository.Update(dbObj);
            UpdateRoleForGroup(group.Id, group.Roles);
        }

        public void UpdateRoleForGroup(int groupId, List<int> roles)
        {
            if (roles == null) roles = new List<int>();
            var roleInDb = _roleRepository.Items.Where(x => x.GroupId == groupId).ToList();
            foreach (var r in roles.Where(x => !roleInDb.Any(y => x == y.RoleId)))
            {
                _roleRepository.Add(new GroupRole()
                {
                    RoleId = r,
                    GroupId = groupId
                });

            }
            foreach (var g in roleInDb.Where(x => !roles.Any(y => y == x.RoleId)))
            {
                _roleRepository.Remove(g);
            }
            _roleRepository.SaveChanges();
        }




        //public void Remove(RolePermissionsDTO role)
        //{
        //    _roleRepository.Remove(Mapper.Map<Role>(role));
        //}
    }
}
