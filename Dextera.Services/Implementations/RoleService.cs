﻿using Dextera.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dextera.Dal.Models;
using Dextera.Dal.Repository;
using Dextera.Dto.Permissions;
using Dextera.Services.Kendo;
using AutoMapper;

namespace Dextera.Services.Implementations
{
    public class RoleService : IRoleService
    {
        public ITenantRepository<Role> _roleRepository;
        public IRepository<Permission> _permissionRepository;
        public IPermissionService _permissionService;
        public ITenantRepository<RolePermission> _rolePermissionRepository;
        public RoleService(ITenantRepository<Role> repository, IRepository<Permission> permissionRepository,
            ITenantRepository<RolePermission> rolePermissionRepository, IPermissionService permissionService)
        {
            _roleRepository = repository;
            _permissionRepository = permissionRepository;
            _permissionService = permissionService;
            _rolePermissionRepository = rolePermissionRepository;
        }

        public DataSourceResult GetList(DataSourceRequest request)
        {
            return _roleRepository.Items
                .ToDataSourceResult<Role, RoleDTO>(request.Take, request.Skip, request.Sort, request.Filter);
        }

        public RolePermissionsDTO Get(int id)
        {
            var res = Mapper.Map<RolePermissionsDTO>(_roleRepository.GetById(id));
            if (res == null)
            {
                res = new RolePermissionsDTO();
            }
            res.Permissions = _permissionRepository.Items.ToList().Select(x => Mapper.Map<PermissionsDTO>(x)).ToList();
            if (id != 0)
            {
                var rolePermissions = _rolePermissionRepository.Items.Where(x => x.RoleId == id);
                foreach(var perm in res.Permissions)
                {
                    perm.Checked = rolePermissions.Any(x => x.PermissionId == perm.Id && x.RoleId == id);
                    if (perm.Checked)
                    {
                        perm.AccessLevel = rolePermissions.FirstOrDefault(x => x.PermissionId == perm.Id && x.RoleId == id).AccessLevel;
                    }
                }
            }
            return res;
        }

        public void Add(RolePermissionsDTO role)
        {
            var id = _roleRepository.Add(Mapper.Map<Role>(role));
            _permissionService.UpdatePermissionsForRole(id, role.Permissions);
        }

        public void Update(RolePermissionsDTO role)
        {
            var dbObj = _roleRepository.GetById(role.Id);
            Mapper.Map<RolePermissionsDTO, Role>(role, dbObj);
            _roleRepository.Update(dbObj);
            _permissionService.UpdatePermissionsForRole(role.Id, role.Permissions);
        }

        public void Remove(RolePermissionsDTO role)
        {
            _roleRepository.Remove(Mapper.Map<Role>(role));
        }
    }
}
