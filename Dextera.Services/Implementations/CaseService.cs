﻿using Dextera.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dextera.Dal.Models;
using Dextera.Dal.Repository;

using AutoMapper;
using Dextera.Dto.Permissions;
using Dextera.Services.Kendo;
using Dextera.Dto.Workflow;

namespace Dextera.Services.Implementations
{
    public class CaseService : ICaseService
    {
        public ITenantRepository<Case> _caseRepository;
        public CaseService(ITenantRepository<Case> repository)
        {
            _caseRepository = repository;
        }

        public DataSourceResult GetList(DataSourceRequest request)
        {
            return _caseRepository.Items
                .ToDataSourceResult<Case, CaseDTO>(request.Take, request.Skip, request.Sort, request.Filter);
        }

        public CaseDTO Get(int id)
        {
            return Mapper.Map<CaseDTO>(_caseRepository.GetById(id));
        }

        public void Add(CaseDTO tenant)
        {
            _caseRepository.Add(Mapper.Map<Case>(tenant));
        }
        public void Update(CaseDTO caseObj)
        {
            var dbObj = _caseRepository.GetById(caseObj.Id);
            Mapper.Map<CaseDTO, Case>(caseObj, dbObj);
            _caseRepository.Update(dbObj);
        }

        public void Remove(CaseDTO caseObj)
        {
            _caseRepository.Remove(Mapper.Map<Case>(caseObj));
        }
    }
}
