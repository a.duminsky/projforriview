﻿using Dextera.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dextera.Dal.Models;
using Dextera.Dal.Repository;
using Microsoft.AspNet.Identity;
using Dextera.Dal;
using Microsoft.AspNet.Identity.EntityFramework;
using Dextera.Services.Kendo;
using Dextera.Dto.Permissions;
using AutoMapper;
using Dextera.Dal.Extensions;

namespace Dextera.Services.Implementations
{
    public class AccountService : IAccountService
    { 
        public DexteraDbContext _context;

        public ITenantRepository<GroupUser> _groupsRepository;
        public ITenantRepository<RoleUser> _roleRepository;
        public DexteraDbContext _dbContext;


        public  UserManager<ApplicationUser> UserManager {
            get
            {
                var userStore = new UserStore<ApplicationUser>(_context);
                var manager = new UserManager<ApplicationUser>(userStore);
                return manager;
            }
        }

        public AccountService(DexteraDbContext context, ITenantRepository<GroupUser> groupsRepository, ITenantRepository<RoleUser> roleRepository,
            DexteraDbContext dbContex)
        {
            _context = context;
            _groupsRepository = groupsRepository;
            _roleRepository = roleRepository;
            _dbContext = dbContex;
        }

        public IQueryable<ApplicationUser> Users
        {
            get
            {
                return _dbContext.Set<ApplicationUser>().SqlQuery("SELECT *, dbo.Decrypt(SNNEncrypted, '123') as SNNEn FROM [dbo].[AspNetUsers]").AsQueryable();
            }
        }


        public DataSourceResult GetList(DataSourceRequest request)
        {
            //request.Sort = new Sort[] { new Sort() { Field = "SNN" } };
            return UserManager.Users.ToDataSourceResult<ApplicationUser, AccountDTO>(request.Take, request.Skip, request.Sort, request.Filter);
        }

        public AccountDTO Get(string id)
        {
            var user = UserManager.FindById(id);
            return Mapper.Map<AccountDTO>(user);
        }

        public void Update(AccountDTO account)
        {
            var user = UserManager.FindById(account.Id);
            Mapper.Map<AccountDTO, ApplicationUser>(account, user);
            var result = UserManager.Update(user);
            UpdateGroupsForUser(user.Id, account.Groups, account.Roles);
            processIdenityResult(result);
        }

        public void Delete(AccountDTO account)
        {
            var res = UserManager.Delete(Mapper.Map<ApplicationUser>(account));
            processIdenityResult(res);
        }

        public void Add(AccountDTO account)
        {
            var user = Mapper.Map<ApplicationUser>(account);
            user.UserName = user.Email;
            user.Id = Guid.NewGuid().ToString();
            var res = UserManager.Create(user, account.Password);
            UpdateGroupsForUser(user.Id, account.Groups, account.Roles);
            processIdenityResult(res);
        }

        private void processIdenityResult(IdentityResult result)
        {
            if (result.Errors.Count() > 0)
            {
                throw new Exception(result.Errors.First());
            }
        }

        public void UpdateGroupsForUser(string userId, List<int> groups, List<int> roles)
        {
            if (groups == null)
            {
                groups = new List<int>();
            }
            if (roles == null)
            {
                roles = new List<int>();
            }
            var groupsInDb = _groupsRepository.Items.Where(x => x.UserId == userId).ToList();
            var rolesInDb = _roleRepository.Items.Where(x => x.UserId == userId).ToList();
            foreach (var g in groups.Where(x => !groupsInDb.Any(y => x == y.GroupId)))
            {
                _groupsRepository.Add(new GroupUser()
                    {
                        GroupId = g,
                        UserId = userId
                });
                
            }
            foreach (var r in roles.Where(x => !rolesInDb.Any(y => x == y.RoleId)))
            {
                _roleRepository.Add(new RoleUser()
                {
                    RoleId = r,
                    UserId = userId
                });

            }




            foreach ( var g in groupsInDb.Where(x => !groups.Any(y => y == x.GroupId)))
            {
                _groupsRepository.Remove(g);
            }
            _groupsRepository.SaveChanges();
            foreach (var r in rolesInDb.Where(x => !groups.Any(y => y == x.RoleId)))
            {
                _roleRepository.Remove(r);
            }
            _roleRepository.SaveChanges();


        }
    }
}
