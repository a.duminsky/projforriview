﻿using Dextera.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dextera.Dal.Models;
using Dextera.Dal.Repository;
using Dextera.Dto.Permissions;

namespace Dextera.Services.Implementations
{
    public class PermissionService : IPermissionService
    {

        public IRepository<Permission> _permissionRepository;
        public ITenantRepository<RolePermission> _rolePermissionRepository;
        public PermissionService(IRepository<Permission> repository, ITenantRepository<RolePermission> rolePermissionRepository)
        {
            _permissionRepository = repository;
            _rolePermissionRepository = rolePermissionRepository;
        }

        public List<Permission> GetList()
        {
            return _permissionRepository.Items.ToList();
        }

        public Permission Get(int id)
        {
            return _permissionRepository.GetById(id);
        }

        public void Add(Permission permission)
        {
            _permissionRepository.Add(permission);
        }
        public void Update(Permission account)
        {
            _permissionRepository.Update(account);
        }

        public void Remove(Permission permission)
        {
            _permissionRepository.Remove(permission);
        }

        public void UpdatePermissionsForRole(int roleId, List<PermissionsDTO> permissions)
        {
            var perms = _rolePermissionRepository.Items.Where(x => x.RoleId == roleId).ToList();
            foreach (var perm in permissions)
            {
                var p = perms.FirstOrDefault(x => x.PermissionId == perm.Id);
                if (perm.Checked && p == null)
                {
                    _rolePermissionRepository.Add(new RolePermission()
                    {
                        PermissionId = perm.Id,
                        RoleId = roleId,
                        AccessLevel = perm.AccessLevel
                    });
                }
                else if (perm.Checked && p != null)
                {
                    if (perm.AccessLevel != p.AccessLevel)
                    {
                        p.AccessLevel = perm.AccessLevel;
                        _rolePermissionRepository.Update(p);
                    }
                }
                else if (!perm.Checked && p != null)
                {
                    _rolePermissionRepository.Remove(p);
                }
            }
            _rolePermissionRepository.SaveChanges();
        }
    }
}
