﻿using Dextera.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dextera.Services.Implementations
{
    public class TestService : ITestService
    {
        public string GetValue()
        {
            return "test";
        }
    }
}
