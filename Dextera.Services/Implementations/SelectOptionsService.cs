﻿using Dextera.Common.Enums;
using Dextera.Dal.Models;
using Dextera.Dal.Repository;
using Dextera.Dto.Common;
using Dextera.Services.Interfaces;
using Dextera.Services.Kendo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dextera.Services.Implementations
{
    public class SelectOptionsService : ISelectOptionsService
    {
        public ITenantRepository<Assessment> _assesmentRepository;
        public ITenantRepository<Attorney> _attorneyRepository;


        public ITenantRepository<Group> _grouRepository;
        public ITenantRepository<Client> _clientRepository;

        public SelectOptionsService(ITenantRepository<Assessment> assesmentRepository, ITenantRepository<Attorney> attorneyRepository,
            ITenantRepository<Group> grouRepository, ITenantRepository<Client> clientRepository)
        {
            _assesmentRepository = assesmentRepository;
            _grouRepository = grouRepository;
            _clientRepository = clientRepository;
            _attorneyRepository = attorneyRepository;
        }
        public DataSourceResult GetOptions(SelectType type, DataSourceRequest request)
        {
            switch(type)
            {
                case SelectType.assessment:
                    return _assesmentRepository.Items.ToDataSourceResult<Assessment, SelectOptionDTO<int>>(request.Take, request.Skip, request.Sort, request.Filter);
                    break;
                case SelectType.group:
                    return _grouRepository.Items.ToDataSourceResult<Group, SelectOptionDTO<int>>(request.Take, request.Skip, request.Sort, request.Filter);
                    break;
                case SelectType.client:
                    return _clientRepository.Items.ToDataSourceResult<Client, SelectOptionDTO<int>>(request.Take, request.Skip, request.Sort, request.Filter);
                    break;
                case SelectType.attorney:
                    return _attorneyRepository.Items.ToDataSourceResult<Attorney, SelectOptionDTO<int>>(request.Take, request.Skip, request.Sort, request.Filter);
                    break;
                default:
                    throw new Exception("Not implemented for " + type);
            }
        }
    }
}
