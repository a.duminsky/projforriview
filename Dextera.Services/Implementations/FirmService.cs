﻿using Dextera.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dextera.Dal.Models;
using Dextera.Dal.Repository;

using AutoMapper;
using Dextera.Dto.Permissions;
using Dextera.Services.Kendo;

namespace Dextera.Services.Implementations
{
    public class FirmService : IFirmService
    {
        public IRepository<Tenant> _tenantRepository;
        public FirmService(IRepository<Tenant> repository)
        {
            _tenantRepository = repository;
        }

        public DataSourceResult GetList(DataSourceRequest request)
        {
            return _tenantRepository.Items
                .ToDataSourceResult<Tenant, TenantDTO>(request.Take, request.Skip, request.Sort, request.Filter);
        }

        public Tenant Get(int id)
        {
            return _tenantRepository.GetById(id);
        }

        public void Add(TenantDTO tenant)
        {
            _tenantRepository.Add(Mapper.Map<Tenant>(tenant));
        }
        public void Update(TenantDTO tenant)
        {
            var dbObj = _tenantRepository.GetById(tenant.Id);
            Mapper.Map<TenantDTO, Tenant>(tenant, dbObj);
            _tenantRepository.Update(dbObj);
        }

        public void Remove(TenantDTO tenant)
        {
            _tenantRepository.Remove(Mapper.Map<Tenant>(tenant));
        }
    }
}
