﻿using Dextera.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dextera.Dal.Models;
using Dextera.Dal.Repository;

using AutoMapper;
using Dextera.Dto.Permissions;
using Dextera.Services.Kendo;
using Dextera.Dto.Workflow;

namespace Dextera.Services.Implementations
{
    public class ClentService : IClientService
    {
        public ITenantRepository<Client> _clientRepository;
        public ClentService(ITenantRepository<Client> repository)
        {
            _clientRepository = repository;
        }

        public DataSourceResult GetList(DataSourceRequest request)
        {
            return _clientRepository.Items
                .ToDataSourceResult<Client, ClientDTO>(request.Take, request.Skip, request.Sort, request.Filter);
        }

        public ClientDTO Get(int id)
        {
            return Mapper.Map<ClientDTO>(_clientRepository.GetById(id));
        }

        public void Add(ClientDTO client)
        {
            _clientRepository.Add(Mapper.Map<Client>(client));
        }
        public void Update(ClientDTO client)
        {
            var dbObj = _clientRepository.GetById(client.Id);
            Mapper.Map<ClientDTO, Client>(client, dbObj);
            _clientRepository.Update(dbObj);
        }

        public void Remove(ClientDTO client)
        {
            _clientRepository.Remove(Mapper.Map<Client>(client));
        }
    }
}
