﻿using Dextera.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dextera.Dal.Models;
using Dextera.Dal.Repository;

using AutoMapper;
using Dextera.Dto.Permissions;
using Dextera.Services.Kendo;
using Dextera.Dto.Workflow;

namespace Dextera.Services.Implementations
{
    public class DefenantService : IDefenantService
    {
        public ITenantRepository<Defenant> _defenantRepository;
        public DefenantService(ITenantRepository<Defenant> repository)
        {
            _defenantRepository = repository;
        }

        public DataSourceResult GetList(DataSourceRequest request)
        {
            return _defenantRepository.Items
                .ToDataSourceResult<Defenant, DefenantDTO>(request.Take, request.Skip, request.Sort, request.Filter);
        }

        public DefenantDTO Get(int id)
        {
            return Mapper.Map<DefenantDTO>(_defenantRepository.GetById(id));
        }

        public void Add(DefenantDTO tenant)
        {
            _defenantRepository.Add(Mapper.Map<Defenant>(tenant));
        }
        public void Update(DefenantDTO caseObj)
        {
            var dbObj = _defenantRepository.GetById(caseObj.Id);
            Mapper.Map<DefenantDTO, Defenant>(caseObj, dbObj);
            _defenantRepository.Update(dbObj);
        }

        public void Remove(DefenantDTO caseObj)
        {
            _defenantRepository.Remove(Mapper.Map<Defenant>(caseObj));
        }
    }
}
