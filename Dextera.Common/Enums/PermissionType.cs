﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dextera.Common.Enums
{
    public enum PermissionType
    {
        MenuItem = 1,
        Widget = 2,
        Custom = 3
    }
}
