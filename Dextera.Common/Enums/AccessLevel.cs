﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dextera.Common.Enums
{
    public enum AccessLevel
    {
        Yes = 1,
        SomeSpecial = 2,
        Some = 3,
        YesSome = 4,
        No = 5,
        Special = 6
    }
}
