﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dextera.Common.Enums
{
    public enum SelectType
    {
        client,
        group,
        role,
        assessment,
        attorney,
        ratePeriod,
        costAdvanceFees,
        feesThat

    }
}
