﻿using Dextera.Dal.Extensions;
using Dextera.Dal.Map;
using Dextera.Dal.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dextera.Dal
{
    public class DexteraDbContext : IdentityDbContext<ApplicationUser>
    {
        public DexteraDbContext() : base("Default")
        {
            this.Configuration.LazyLoadingEnabled = false;
            Database.SetInitializer(new DbInitializer());
            
        }
        //public DexteraDbContext(string securityPartition = null, string asRole = null) : base("Default")
        //{
        //    Database.SetInitializer(new DbInitializer());
            
        //}



        public DbSet<RolePermission> RolePermissions { get; set; }

        public DbSet<Permission> Permissions { get; set; }

        public DbSet<Role> Role { get; set; }

        public DbSet<Tenant> Tenants { get; set; }
        public DbSet<Group> Group { get; set; }
        public DbSet<GroupUser> GroupUser { get; set; }
        public DbSet<GroupRole> GroupRole { get; set; }

        public DbSet<Assessment> Assessments { get; set; }
        public DbSet<Attorney> Attorney { get; set; }
        public DbSet<Case> Cases { get; set; }
        public static DexteraDbContext Create()
        {
            return new DexteraDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            //modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Conventions.Add(new DecryptMethodInjection());
            
        }
    }
}
