﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dextera.Dal.Models
{
    public class Case : TenantEntity
    {
        public string AddressStreet { get; set; }
        public string AddressUnit { get; set; }

        public string AddressCity { get; set; }
        public string AddressState { get; set; }
        public string AddressZip { get; set; }
        public string AddressPhone { get; set; }

        public int? ClientId { get; set; }

        public virtual Client Client { get; set; }

        public bool AdvanceRetainerReceived { get; set; }

        public List<Defenant> Defenants { get; set; }
    }
}
