﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Dextera.Dal.Models;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Dextera.Dal.Models
{
   // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class RoleUser : TenantEntity
    {
        public int RoleId { get; set; }
        [Required]
        public string UserId { get; set; }
        public virtual Role Role { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}