﻿using Dextera.Dal.Models.Dictionaries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dextera.Dal.Models
{
    public class Defenant : TenantEntity
    {
        public int ClientId { get; set; }
        public string FirstName { get; set; }

        public string MiddleName { get; set; }
        public string LastName { get; set; }


        public int? GenderId { get; set; }
        public Gender Gender { get;set;}

        public double? Height { get; set; }

        public int? HairId { get; set; }
        public Hair Hair { get; set; }

        public int? EyesId { get; set; }
        public Eyes Eyes { get; set; }

        public int? EthnicId { get; set; }
        public Ethnic Ethnic { get; set; }

        public string AddressStreet { get; set; }
        public string AddressUnit { get; set; }

        public string AddressCity { get; set; }
        public string AddressState { get; set; }
        public string AddressZip { get; set; }
        public string AddressPhone { get; set; }
    }
}
