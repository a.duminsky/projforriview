﻿using Dextera.Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dextera.Dal.Models
{
    public class UserPermission : TenantEntity
    {
        [Key]
        public int Id { get; set; }


        [Required]
        public int PermissionId { get; set; }

        public virtual Permission Permission { get; set; }

        public AccessLevel AccessLevel { get; set; }
    }

    
}
