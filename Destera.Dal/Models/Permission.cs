﻿using Dextera.Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dextera.Dal.Models
{
    public class Permission : BaseEntity
    {
        public string Title { get; set; }
        public PermissionType Type { get; set; }

        public bool AccessLevelRequire { get; set; }

       
    }

}

