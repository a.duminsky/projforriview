﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Dextera.Dal.Models;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

using Dextera.Common.Attributes;
using System.ComponentModel.DataAnnotations.Schema;
using Dextera.Dal.Extensions;

namespace Dextera.Dal.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        
        public string Hometown { get; set; }

        public int? TenantId { get; set; }

        [EncriptedField]
        [NotMapped]
        public string SNN
        {
            get { return SNNEncrypted;  }
            set { SNNEncrypted = value; }
        }

        public string Decrypt(string value, string key)
        {
            return "";
        }



        public string SNNEncrypted { get; set; }

        private string fullname = null;

        /*  <summary>
            gets or sets first name
            </summary>
            <value></value>
            <returns></returns>
            <remarks></remarks>*/
        //[Required]
        //[StringLength(50, ErrorMessage = "First name cannot be longer than 50 characters")]
        //[Display(Name = "First Name")]
        public string FirstName { get; set; }

        /*  <summary>
            gets or sets last name
            </summary>
            <value></value>
            <returns></returns>
            <remarks></remarks>*/
        //[Required]
        //[StringLength(50, ErrorMessage = "Last name cannot be longer than 50 characters")]
        //[Display(Name = "Last Name ")]
        public string LastName { get; set; }

        /*  <summary>
        gets or sets Phone
        </summary>
        <value></value>
        <returns></returns>
        <remarks></remarks>*/

        [StringLength(40, ErrorMessage = "Phone cannot be longer than 40 characters")]
        [Display(Name = "Phone")]
        public string Phone { get; set; }

        /*  <summary>
        gets or sets PhoneExt
        </summary>
        <value></value>
        <returns></returns>
        <remarks></remarks>*/
        [StringLength(20, ErrorMessage = "Phone Ext. cannot be longer than 20 characters")]
        [Display(Name = "Ext.")]
        public string PhoneExt { get; set; }

        /*  <summary>
            gets or sets email
            </summary>
            <value></value>
            <returns></returns>
            <remarks></remarks>*/
        //[Required]
        //[StringLength(50)]
        //[Display(Name = "Email ")]
        //[DataType(DataType.EmailAddress)]
        //[RegularExpression("^(([^<>()[\\]\\.,;:\\s@\"]+(\\.[^<>()[\\]\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$", ErrorMessage = "Email is not valid e-mail address.")]
        //public string Email { get; set; }

        ///*  <summary>
        //    gets or sets User Name
        //    </summary>
        //    <value></value>
        //    <returns></returns>
        //    <remarks></remarks>*/

        //[Required]
        //[StringLength(50, ErrorMessage = "User name cannot be longer than 50 characters")]
        //[Display(Name = "User Name ")]
        //public string UserName { get; set; }

        /*  <summary>
            gets or sets password
            </summary>
            <value></value>
            <returns></returns>
            <remarks></remarks>*/
        //[Required]
        //[StringLength(50)]
        //[Display(Name = "Password ")]
        //public string Password { get; set; }

        /*  <summary>
            gets or sets salt
            </summary>
            <value></value>
            <returns></returns>
            <remarks></remarks>*/
        [StringLength(4)]
        public string salt { get; set; }

        /*  <summary>
            gets or sets user active
            </summary>
            <value></value>
            <returns></returns>
            <remarks></remarks>*/
        public bool useractive { get; set; }

        /*  <summary>
            gets or sets reset password
            </summary>
            <value></value>
            <returns></returns>
            <remarks></remarks>*/
        public bool resetpassword { get; set; }

        /*  <summary>
            gets or sets Alias
            </summary>
            <value></value>
            <returns></returns>
            <remarks></remarks>*/
        [StringLength(50)]
        [Display(Name = "Alias ")]
        public string Alias { get; set; }

        /*  <summary>
            gets or sets BAR
            </summary>
            <value></value>
            <returns></returns>
            <remarks></remarks>*/
        [StringLength(40)]
        [Display(Name = "BAR# ")]
        public string Bar { get; set; }

        /*  <summary>
            gets or sets MI
            </summary>
            <value></value>
            <returns></returns>
            <remarks></remarks>*/
        [StringLength(40)]
        [Display(Name = "IM")]
        public string IM { get; set; }

        /*  <summary>
            gets or sets FullName
            </summary>
            <value></value>
            <returns></returns>
            <remarks></remarks>*/
        [Display(Name = "Full Name ")]
        public string FullName
        {
            get
            {
                if (LastName != null && FirstName != null)
                    return LastName + " " + FirstName;
                if (LastName == null)
                    return FirstName;
                if (FirstName == null)
                    return LastName;
                else
                    return null;
            }
            set
            {
                fullname = value;
            }
        }

      
        //public List<Role> Roles { get; set; }


        public virtual List<UserPermission> UserPermissions { get; set; }

        public virtual List<GroupUser> GroupUsers { get; set; }
        public virtual List<RoleUser> RoleUser { get; set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

 
}