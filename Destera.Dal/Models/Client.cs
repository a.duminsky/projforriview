﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Dextera.Dal.Models;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dextera.Dal.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class Client : TenantEntity
    {
        //Client 
        public string ClientName { get; set; }
        public string ClientType { get; set; } // Вопрос а как быть с уже существующм юзеро с полями его?
        public string ClientAddress { get; set; }
        public string CityStateZip { get; set; }
        public string GeneralPhone { get; set; }
        public string Fax { get; set; }
        public string Website { get; set; }
        public string LegalName { get; set; }
        public int? AttorneyId { get; set; }
        [ForeignKey("AttorneyId")]
        public virtual Attorney Attorney { get; set; }  // ?? ++ add table
        public bool Annual { get; set; }
        public int? SubClientId { get; set; }
        public DateTime? Signed { get; set; }
        public virtual Client SubClient { get; set; } //add subclient table

        //Contact

        // name, role? откуда брать? И все поля должны с юзер совпадать? - (mradetsky) - не понял вопроса
        public string Address { get; set; }
        public string CityStateZip4Second { get; set; }
        public string PhoneExt { get; set; }
        public string Mobile { get; set; }
        public string EmailContact { get; set; }
        public int? RoleId { get; set; }
        public virtual Role Role { get; set; } // приавльно подключил?(mradetsky) - если нужноа одна роль да, если несколько нет, я вообще сомниваюсь что тут нужны роли.... Каким боком клиент к ролям...

        //Financial
        public string CostAdvanceAmountMoney { get; set; }
        public bool CostAdvanceRB { get; set; }
        public string RetainerAmountMoney { get; set; }
        public bool RetainerRB { get; set; }
        public bool AssessmentFeesRB { get; set; }
        public string AssessmentMoney { get; set; }
        public int? AssessmentId { get; set; }
        [ForeignKey("AssessmentId")]
        public virtual Assessment Assessment { get; set; }  // ?? ++ add table
        public string HOAInterestPercent { get; set; } // ?? такой тип - (mradetsky) - читай тз
        public string LateFeeMoney { get; set; } // ?? ++ add table - (mradetsky) - читай тз если там дроп даун то да если нет то нет....
        public string LateFeePercent { get; set; }
        public string AssessmentIncMoney { get; set; }
        public string AssessmentIncPercent { get; set; }
        public bool InterestIncreaseRB { get; set; }
        public string InterestIncreasePercent { get; set; }
        public bool LateFeeIncreaseRB { get; set; }
        public string LateFeeIncreaseMoney { get; set; }
        public DateTime? IncreaseStartDate { get; set; }
        public string SpecialAssessmentMoney { get; set; }
        public DateTime? SpecialAssessmentStartDate { get; set; }
        public DateTime? SpecialAssessmentEndDate { get; set; }
        public int? SpecialAssessmentId { get; set; }
        public virtual Assessment SpecialAssessment { get; set; }  // ?? ++ add table - (mradetsky) - да, но мы с тобой обсудили что дропдауны с одинковым напором опций в обду новую таблицу, а ты сделал в разных....
        public bool SpecialBillingRB { get; set; }
        public string AttorneyRateMoney { get; set; }
        public int? AttorneyRateId { get; set; }
        [ForeignKey("AttorneyRateId")]
        public virtual RatePeriod AttorneyRate { get; set; }  // ?? ++ add table (mradetsky) - да, но мы с тобой обсудили что дропдауны с одинковым напором опций в обду новую таблицу, а ты сделал в разных....
        public string CourtFeesMoney { get; set; }
        public int? CourtFeesId { get; set; }
        [ForeignKey("CourtFeesId")]
        public virtual RatePeriod CourtFees { get; set; }  // ?? ++ add table (mradetsky) - да, но мы с тобой обсудили что дропдауны с одинковым напором опций в обду новую таблицу, а ты сделал в разных....
        public string CostAdvanceFeesMoney { get; set; }
        public int? CostAdvanceFeesId { get; set; }
        public virtual CostAdvanceFees CostAdvanceFees { get; set; }  // ?? ++ add table

        //Settlement Provisions
        public int? LengthPaymentId { get; set; }
        public virtual LengthPayment LengthPayment { get; set; }  // ?? ++ add table
        public string DownPaymentMoney { get; set; }
        public string DownPaymentPercent { get; set; }
        public int? FeesThatId { get; set; }
        public virtual FeesThat FeesThat { get; set; }  // ?? ++ add table

        public string WillSettlePercent { get; set; }
        public int? WillSettleId { get; set; }
        public virtual WillSettle WillSettle { get; set; }  // ?? ++ add table



        


        /// <summary>
        /// (mradetsky) - на будущее:
        /// 1 ты должен минимум раз в день запускать(а лучше несколько раз) и проверять то что ты делаеш, если ты неделю делаеш форму а потом только ее запускаеш и чиниш - ты делаеш что-то не так.
        /// 2 Как только ты запустил и проверил(пускай не все работает, но не ломает старое хотя бы) - залей изменения.
        /// 3 Делаеш миграции - согласовуй и заливай.
        /// 4 если видиш много подобных контролов(или просто кусков) где будет много подобного кода - подумай как минимизировать дублирование, что-то вынести в отдельную функцию/сервис/контрол. Даже если копипастом быстре....

        /// </summary>
    }
}