﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dextera.Dal.Models
{
    public class Role : TenantEntity
    {   
        public string Name { get; set; }

        public virtual List<RolePermission> Permissions { get; set; }
        public virtual List<RoleUser> RoleUser { get; set; }

    }
}
