﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dextera.Dal.Models
{
    public class TenantEntity : BaseEntity
    {
        public int TenantId { get; set; }
    }
}
