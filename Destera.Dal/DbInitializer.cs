﻿using Dextera.Dal.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dextera.Dal
{
    public class DbInitializer : CreateDatabaseIfNotExists<DexteraDbContext>
    {
        public static void SeedTestData(DexteraDbContext context)
        {
            SeedStaticData(context);
        }

        protected override void Seed(DexteraDbContext context)
        {
            FillUsers(context);
            SeedStaticData(context);
            base.Seed(context);
        }

        private static void SeedStaticData(DexteraDbContext context)
        {
            context.Users.Add(new ApplicationUser());
            context.SaveChanges();
        }

        private static void FillUsers(DexteraDbContext context)
        {
            var userStore = new UserStore<ApplicationUser>();
            var manager = new UserManager<ApplicationUser>(userStore);
            userStore.AutoSaveChanges = true;
            CreateUserIfNotExist(new ApplicationUser() { Email = "test@volya.com" }, manager);
            
        }

        public static void CreateUserIfNotExist(ApplicationUser user, UserManager<ApplicationUser> manager)
        {
            Trace.WriteLine("I'm seeding!");
            var dbUser = manager.FindByEmail(user.Email);
            if (dbUser == null)
            {
                var res = manager.Create(user, "password");
            }
            

        }
    }
}
