﻿using Dextera.Dal.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dextera.Dal.Map
{
    public class UserMap : EntityTypeConfiguration<ApplicationUser>
    {
        public UserMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            //Property(t => t.SNNEncrypted);
            
            // Table & Column Mappings
            ToTable("AspNetUsers");
            //Property(t => t.SNNEncrypted).HasColumnName("SNNEncrypted");
            //Property(t=> t.SNN).HasComputedColumnSql()

                //modelBuilder.Entity(entity =>
                //{
                //    entity.Property(e => e.OrderTotalComputed)
                //    .HasColumnType("money")
                //    .HasComputedColumnSql("Store.GetOrderTotal([Id])");
                //    //.ValueGeneratedOnAddOrUpdate();
                //});

        }
    }
}
