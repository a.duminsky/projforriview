﻿-- ================================================
-- Template generated from Template Explorer using:
-- Create Inline Function (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
 
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID (N'Encrypt', N'FN') IS NOT NULL  
    DROP FUNCTION Encrypt;  
GO 

CREATE FUNCTION Encrypt
(	
	-- Add the parameters for the function here
	@val nvarchar(max), 
	@key nvarchar(max)
)
RETURNS nvarchar(max)
AS
 
BEGIN
	-- Add the SELECT statement with parameter references here
	RETURN @val;
END
GO



IF OBJECT_ID (N'Decrypt', N'FN') IS NOT NULL  
    DROP FUNCTION Decrypt;  
GO 

CREATE FUNCTION Decrypt
(	
	-- Add the parameters for the function here
	@val nvarchar(max), 
	@key nvarchar(max)
)
RETURNS nvarchar(max)
AS
 
BEGIN
	-- Add the SELECT statement with parameter references here
	RETURN @val;
END
GO
