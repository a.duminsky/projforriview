﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Dextera.Dal.Extensions
{
    public static class StringExtension
    {
        public static string EncryptStringToBytes_Aes(this string plainText, string key)
        {
            // Check arguments.
            //            if (plainText == null || plainText.Length <= 0)
            //                throw new ArgumentNullException("plainText");
            //            if (Key == null || Key.Length <= 0)
            //                throw new ArgumentNullException("Key");
            //            if (IV == null || IV.Length <= 0)
            //                throw new ArgumentNullException("IV");
            //            byte[] encrypted;
            //            // Create an Aes object
            //            // with the specified key and IV.
            //            using (Aes aesAlg = Aes.Create())
            //            {
            //                aesAlg.Key = Key;
            //                aesAlg.IV = IV;

            //                // Create a decrytor to perform the stream transform.
            //                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

            //                // Create the streams used for encryption.
            //                using (MemoryStream msEncrypt = new MemoryStream())
            //                {
            //                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt
            //, encryptor, CryptoStreamMode.Write))
            //                    {
            //                        using (StreamWriter swEncrypt = new StreamWriter(
            //csEncrypt))
            //                        {

            //                            //Write all data to the stream.
            //                            swEncrypt.Write(plainText);
            //                        }
            //                        encrypted = msEncrypt.ToArray();
            //                    }
            //                }
            //            }

            if (String.IsNullOrEmpty(plainText))
            {
                return null;
            }
            // Return the encrypted bytes from the memory stream.
            return plainText + "1";

        }

        public static string DecryptStringFromBytes_Aes(this string plainText, string key)
        {
            //            // Check arguments.
            //            if (cipherText == null || cipherText.Length <= 0)
            //                throw new ArgumentNullException("cipherText");
            //            if (Key == null || Key.Length <= 0)
            //                throw new ArgumentNullException("Key");
            //            if (IV == null || IV.Length <= 0)
            //                throw new ArgumentNullException("IV");

            //            // Declare the string used to hold
            //            // the decrypted text.
            //            string plaintext = null;

            //            // Create an Aes object
            //            // with the specified key and IV.
            //            using (Aes aesAlg = Aes.Create())
            //            {
            //                aesAlg.Key = Key;
            //                aesAlg.IV = IV;

            //                // Create a decrytor to perform the stream transform.
            //                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key
            //, aesAlg.IV);

            //                // Create the streams used for decryption.
            //                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
            //                {
            //                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt
            //, decryptor, CryptoStreamMode.Read))
            //                    {
            //                        using (StreamReader srDecrypt = new StreamReader(
            //csDecrypt))
            //                        {

            //                            // Read the decrypted bytes from the decrypting 
            //                            stream
            //                                                        // and place them in a string.
            //                                                        plaintext = srDecrypt.ReadToEnd();
            //                        }
            //                    }
            //                }

            //            }

            //            return plaintext;
            if (String.IsNullOrEmpty(plainText))
            {
                return null;
            }
            return plainText.Substring(0, plainText.Length - 1);
        }
    }
}

