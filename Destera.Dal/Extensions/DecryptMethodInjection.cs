﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Infrastructure;

namespace Dextera.Dal.Extensions
{
    public class DecryptMethodInjection : IStoreModelConvention<EdmModel>
    {
        public void Apply(EdmModel item, DbModel model)
        {
            var parameter = FunctionParameter.Create(
                "value",
                model.GetStorePrimitiveType(PrimitiveTypeKind.String),
                ParameterMode.In);

            var parameter2 = FunctionParameter.Create(
                "key",
                model.GetStorePrimitiveType(PrimitiveTypeKind.String),
                ParameterMode.In);

            var res = FunctionParameter.Create(
                "result",
                model.GetStorePrimitiveType(PrimitiveTypeKind.String),
                ParameterMode.ReturnValue);

            var function = item.CreateAndAddFunction(
                "Decrypt",
                new[] { parameter, parameter2 },
                new[] { res }
                );
        }
    }
}
