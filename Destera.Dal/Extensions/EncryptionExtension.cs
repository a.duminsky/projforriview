﻿//using Dextera.Dal.Models;
//using System;
//using System.Collections.Generic;
//using System.Data.Entity.Core.Metadata.Edm;
//using System.Data.Entity.ModelConfiguration.Conventions;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace Dextera.Dal.Extensions
//{
//    public class EncryptionExtension : IStoreModelConvention<EdmModel>
//    {
//        public static readonly EncryptionExtension Instance = new EncryptionExtension();

//        public void Apply(EdmModel item, ApplicationUser model)
//        {
//            var valueParameter = FunctionParameter.Create("column", this.GetStorePrimitiveType(model, PrimitiveTypeKind.String), ParameterMode.In);
//            var formatParameter = FunctionParameter.Create("value", this.GetStorePrimitiveType(model, PrimitiveTypeKind.String), ParameterMode.In);
//            var returnValue = FunctionParameter.Create("result", this.GetStorePrimitiveType(model, PrimitiveTypeKind.Boolean), ParameterMode.ReturnValue);

//            var function = this.CreateAndAddFunction(item, "TRIM", new[] { valueParameter, formatParameter }, new[] { returnValue });
//        }

//        protected EdmFunction CreateAndAddFunction(EdmModel item, String name, IList<FunctionParameter> parameters, IList<FunctionParameter> returnValues)
//        {
//            var payload = new EdmFunctionPayload { StoreFunctionName = name, Parameters = parameters, ReturnParameters = returnValues, Schema = this.GetDefaultSchema(item), IsBuiltIn = true };
//            var function = EdmFunction.Create(name, this.GetDefaultNamespace(item), item.DataSpace, payload, null);

//            item.AddItem(function);

//            return (function);
//        }

//        protected EdmType GetStorePrimitiveType(ApplicationUser model, PrimitiveTypeKind typeKind)
//        {
//            return (model.ProviderManifest.GetStoreType(TypeUsage.CreateDefaultTypeUsage(PrimitiveType.GetEdmPrimitiveType(typeKind))).EdmType);
//        }

//        protected String GetDefaultNamespace(EdmModel layerModel)
//        {
//            return (layerModel.GlobalItems.OfType<EdmType>().Select(t => t.NamespaceName).Distinct().Single());
//        }

//        protected String GetDefaultSchema(EdmModel layerModel)
//        {
//            return (layerModel.Container.EntitySets.Select(s => s.Schema).Distinct().SingleOrDefault());
//        }
//    }
//}
