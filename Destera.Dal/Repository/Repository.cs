﻿using Dextera.Dal.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dextera.Dal.Repository
{
    public class Repository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly DexteraDbContext m_dbContext;
        private readonly IMultitenancyService _multitenancyService;
        private readonly DbSet<T> m_table;

        public Repository(DexteraDbContext dbContext, IMultitenancyService multitenancyService)
        {
            m_dbContext = dbContext;
            m_table = m_dbContext.Set<T>();
            _multitenancyService = multitenancyService;


        }

       



        public IQueryable<T> Items
        {
            get
            {
                return m_table;
            }
        }



        public int Add(T entity)
        {
            entity.CreatedAt = DateTime.UtcNow;
            entity.UpdatedAt = DateTime.UtcNow;
            entity.CreatedBy = _multitenancyService.UserId;
            entity.UpdatedBy = _multitenancyService.UserId;
            var obj = m_table.Add(entity);
            m_dbContext.SaveChanges();
            return obj.Id;
        }


        public void Add(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                entity.CreatedAt = DateTime.UtcNow;
                entity.UpdatedAt = DateTime.UtcNow;
                entity.CreatedBy = _multitenancyService.UserId;
                entity.UpdatedBy = _multitenancyService.UserId;
            }
            m_table.AddRange(entities);
            m_dbContext.SaveChanges();
        }

        public T GetById(int id)
        {
            return Items.FirstOrDefault(x => x.Id == id);
        }

        public void Remove(T entity)
        {
            if (Items.Any(x => x.Id == entity.Id))
            {
                m_table.Remove(entity);
                m_dbContext.SaveChanges();
            }
            else
            {
                throw new Exception("Entity not found or you have no access to it!");
            }
        }

        public void Update(T entity)
        {
            
            entity.UpdatedAt = DateTime.UtcNow;
            entity.UpdatedBy = _multitenancyService.UserId;
            if (Items.Any(x => x.Id == entity.Id))
            {
                m_table.AddOrUpdate(i => i.Id, entity);
                m_dbContext.SaveChanges();
            }
            else
            {
                throw new Exception("Entity not found or you have no access to it!");
            }
        }

        public void SaveChanges()
        {
            m_dbContext.SaveChanges();

        }

        
    }
}
