﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dextera.Dal.Repository
{
    public interface IMultitenancyService
    {
        int? TenantId {get;}

        string UserId { get; }

        void UpdateUser();

        void SetUser(string name);
    }
}
