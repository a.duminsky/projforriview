﻿using Dextera.Dal.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dextera.Dal.Repository
{
    public class TenantRepository<T> : ITenantRepository<T> where T : TenantEntity
    {
        private readonly DexteraDbContext m_dbContext;
        private readonly IMultitenancyService _multitenancyService;
        private readonly DbSet<T> m_table;
        private readonly IRepository<T> _repository;

        public TenantRepository(DexteraDbContext dbContext, IMultitenancyService multitenancyService, IRepository<T> repository)
        {
            m_dbContext = dbContext;
            m_table = m_dbContext.Set<T>();
            _multitenancyService = multitenancyService;
            _repository = repository;
        }

        private bool IsTenancyEntity
        {
            get
            {
                return (typeof(T).GetType().GetProperties().Any(x => x.Name == "TenantId"));
            }
        }



        public IQueryable<T> Items
        {
            get
            {
                var item = Activator.CreateInstance<T>();
                if (IsTenancyEntity)
                {
                    var query = m_table.AsQueryable();
                    
                    return query.Where(x => x.TenantId == _multitenancyService.TenantId);
                    
                }
                return _repository.Items;
            }
        }



        public int Add(T entity)
        {
            if (IsTenancyEntity)
            {
                if (!_multitenancyService.TenantId.HasValue)
                {
                    throw new Exception("Can not create entity without tenant!");
                }
                entity.TenantId = _multitenancyService.TenantId.Value;
            }
            return _repository.Add(entity);
        }


        public void Add(IEnumerable<T> entities)
        {
            if (IsTenancyEntity)
            {
                if (!_multitenancyService.TenantId.HasValue)
                {
                    throw new Exception("Can not create entity without tenant!");
                }
                foreach (var en in entities)
                {
                    en.TenantId = _multitenancyService.TenantId.Value;
                }
            }
            _repository.Add(entities);
        }

        public T GetById(int id)
        {
            return _repository.GetById(id);
        }

        public void Remove(T entity)
        {
            if (Items.Any(x => x.Id == entity.Id))
            {
                _repository.Remove(entity);
            }
            else
            {
                throw new Exception("Entity not found or you have no access to it!");
            }
        }

        public void Update(T entity)
        {
            if (Items.Any(x => x.Id == entity.Id))
            {
                _repository.Update(entity);
            }
            else
            {
                throw new Exception("Entity not found or you have no access to it!");
            }
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();

        }

        
    }
}
