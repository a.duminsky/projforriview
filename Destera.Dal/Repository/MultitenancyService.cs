﻿using Dextera.Dal.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dextera.Dal.Repository
{
    public class MultitenancyService : IMultitenancyService
    {
        private DexteraDbContext _dbContext;
        public MultitenancyService(DexteraDbContext dbContext)
        {
            _dbContext = dbContext;
            
        }
        private ApplicationUser _user;
        private string _username;

        public void UpdateUser()
        {
            if (_user == null || _user.UserName != _username)
            {
                var userStore = new UserStore<ApplicationUser>(_dbContext);
                var manager = new UserManager<ApplicationUser>(userStore);
                _user = manager.Users.FirstOrDefault(u => u.UserName == _username);
            }
        }

        public void SetUser(string userName)
        {
            _username = userName;
        }

        public string UserId { get {
                UpdateUser();
                if (_user != null)
                {
                    return _user.Id;
                }
                else
                {
                    return null;
                }

            } 
        }
        
        public int? TenantId { get {
                UpdateUser();
                if (_user != null)
                {
                    return _user.TenantId;
                }
                else
                {
                    return null;
                }

            }

        }
    }
}
