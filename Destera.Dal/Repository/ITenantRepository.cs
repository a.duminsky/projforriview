﻿using Dextera.Dal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dextera.Dal.Repository
{
    public interface ITenantRepository<T> where T : TenantEntity
    {
        IQueryable<T> Items { get; }

        int Add(T entity);
        void Add(IEnumerable<T> entities);
        T GetById(int id);
        void Remove(T entity);
        void Update(T entity);
        void SaveChanges();
    }
}
