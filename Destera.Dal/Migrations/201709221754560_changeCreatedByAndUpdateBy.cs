namespace Dextera.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeCreatedByAndUpdateBy : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Accounts", "UpdatedBy", c => c.String());
            AlterColumn("dbo.AccountContacts", "CreatedBy", c => c.String());
            AlterColumn("dbo.AccountContacts", "UpdatedBy", c => c.String());
            AlterColumn("dbo.AccountManagementCompanies", "CreatedBy", c => c.String());
            AlterColumn("dbo.AccountManagementCompanies", "UpdatedBy", c => c.String());
            AlterColumn("dbo.AccountPreAuthorizedActions", "CreatedBy", c => c.String());
            AlterColumn("dbo.AccountPreAuthorizedActions", "UpdatedBy", c => c.String());
            AlterColumn("dbo.Contacts", "CreatedBy", c => c.String());
            AlterColumn("dbo.Contacts", "UpdatedBy", c => c.String());
            AlterColumn("dbo.Emails", "CreatedBy", c => c.String());
            AlterColumn("dbo.Emails", "UpdatedBy", c => c.String());
            AlterColumn("dbo.ManagerDesignations", "CreatedBy", c => c.String());
            AlterColumn("dbo.ManagerDesignations", "UpdatedBy", c => c.String());
            AlterColumn("dbo.Phones", "CreatedBy", c => c.String());
            AlterColumn("dbo.Phones", "UpdatedBy", c => c.String());
            AlterColumn("dbo.FeeWaiveds", "CreatedBy", c => c.String());
            AlterColumn("dbo.FeeWaiveds", "UpdatedBy", c => c.String());
            AlterColumn("dbo.ManagementCompanies", "CreatedBy", c => c.String());
            AlterColumn("dbo.ManagementCompanies", "UpdatedBy", c => c.String());
            AlterColumn("dbo.ManagCoRoles", "CreatedBy", c => c.String());
            AlterColumn("dbo.ManagCoRoles", "UpdatedBy", c => c.String());
            AlterColumn("dbo.ManagCoManagers", "CreatedBy", c => c.String());
            AlterColumn("dbo.ManagCoManagers", "UpdatedBy", c => c.String());
            AlterColumn("dbo.PreAuthorizedActions", "CreatedBy", c => c.String());
            AlterColumn("dbo.PreAuthorizedActions", "UpdatedBy", c => c.String());
            AlterColumn("dbo.Attorneys", "CreatedBy", c => c.String());
            AlterColumn("dbo.Attorneys", "UpdatedBy", c => c.String());
            AlterColumn("dbo.Banks", "CreatedBy", c => c.String());
            AlterColumn("dbo.Banks", "UpdatedBy", c => c.String());
            AlterColumn("dbo.MenuModels", "CreatedBy", c => c.String());
            AlterColumn("dbo.MenuModels", "UpdatedBy", c => c.String());
            AlterColumn("dbo.ProcessServicers", "CreatedBy", c => c.String());
            AlterColumn("dbo.ProcessServicers", "UpdatedBy", c => c.String());
            AlterColumn("dbo.Recorders", "CreatedBy", c => c.String());
            AlterColumn("dbo.Recorders", "UpdatedBy", c => c.String());
            AlterColumn("dbo.Roles", "CreatedBy", c => c.String());
            AlterColumn("dbo.Roles", "UpdatedBy", c => c.String());
            AlterColumn("dbo.RolePermissions", "CreatedBy", c => c.String());
            AlterColumn("dbo.RolePermissions", "UpdatedBy", c => c.String());
            AlterColumn("dbo.Permissions", "CreatedBy", c => c.String());
            AlterColumn("dbo.Permissions", "UpdatedBy", c => c.String());
            AlterColumn("dbo.Sheriffs", "CreatedBy", c => c.String());
            AlterColumn("dbo.Sheriffs", "UpdatedBy", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Sheriffs", "UpdatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.Sheriffs", "CreatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.Permissions", "UpdatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.Permissions", "CreatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.RolePermissions", "UpdatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.RolePermissions", "CreatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.Roles", "UpdatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.Roles", "CreatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.Recorders", "UpdatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.Recorders", "CreatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.ProcessServicers", "UpdatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.ProcessServicers", "CreatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.MenuModels", "UpdatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.MenuModels", "CreatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.Banks", "UpdatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.Banks", "CreatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.Attorneys", "UpdatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.Attorneys", "CreatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.PreAuthorizedActions", "UpdatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.PreAuthorizedActions", "CreatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.ManagCoManagers", "UpdatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.ManagCoManagers", "CreatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.ManagCoRoles", "UpdatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.ManagCoRoles", "CreatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.ManagementCompanies", "UpdatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.ManagementCompanies", "CreatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.FeeWaiveds", "UpdatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.FeeWaiveds", "CreatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.Phones", "UpdatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.Phones", "CreatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.ManagerDesignations", "UpdatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.ManagerDesignations", "CreatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.Emails", "UpdatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.Emails", "CreatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.Contacts", "UpdatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.Contacts", "CreatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.AccountPreAuthorizedActions", "UpdatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.AccountPreAuthorizedActions", "CreatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.AccountManagementCompanies", "UpdatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.AccountManagementCompanies", "CreatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.AccountContacts", "UpdatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.AccountContacts", "CreatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.Accounts", "UpdatedBy", c => c.Int(nullable: false));
        }
    }
}
