namespace Dextera.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using System.IO;

    public partial class EncryptFunctions : DbMigration
    {
        public override void Up()
        {
            var sqlFile = AppDomain.CurrentDomain.BaseDirectory + @"../../sql/encryptionFunctions.sql";
            Sql(File.ReadAllText(sqlFile));
        }
        
        public override void Down()
        {
        }
    }
}
