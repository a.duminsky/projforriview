namespace Dextera.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RolePermissionRelation_Fix2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RolePermissions", "RoleId", c => c.Int(nullable: false));
            CreateIndex("dbo.RolePermissions", "RoleId");
            AddForeignKey("dbo.RolePermissions", "RoleId", "dbo.Roles", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RolePermissions", "RoleId", "dbo.Roles");
            DropIndex("dbo.RolePermissions", new[] { "RoleId" });
            DropColumn("dbo.RolePermissions", "RoleId");
        }
    }
}
