namespace Dextera.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RolePermissionRelation_Fix : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Permissions", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.RolePermissions", "Role_Id", "dbo.Roles");
            DropForeignKey("dbo.RolePermissions", "Permission_Id", "dbo.Permissions");
            DropIndex("dbo.Permissions", new[] { "RoleId" });
            DropIndex("dbo.RolePermissions", new[] { "Permission_Id" });
            DropIndex("dbo.RolePermissions", new[] { "Role_Id" });
            DropColumn("dbo.RolePermissions", "PermissionId");
            RenameColumn(table: "dbo.RolePermissions", name: "Permission_Id", newName: "PermissionId");
            AlterColumn("dbo.RolePermissions", "PermissionId", c => c.Int(nullable: false));
            AlterColumn("dbo.RolePermissions", "PermissionId", c => c.Int(nullable: false));
            CreateIndex("dbo.RolePermissions", "PermissionId");
            AddForeignKey("dbo.RolePermissions", "PermissionId", "dbo.Permissions", "Id", cascadeDelete: true);
            DropColumn("dbo.Permissions", "RoleId");
            DropColumn("dbo.RolePermissions", "RoleId");
            DropColumn("dbo.RolePermissions", "Role_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.RolePermissions", "Role_Id", c => c.Int());
            AddColumn("dbo.RolePermissions", "RoleId", c => c.String(nullable: false));
            AddColumn("dbo.Permissions", "RoleId", c => c.Int(nullable: false));
            DropForeignKey("dbo.RolePermissions", "PermissionId", "dbo.Permissions");
            DropIndex("dbo.RolePermissions", new[] { "PermissionId" });
            AlterColumn("dbo.RolePermissions", "PermissionId", c => c.Int());
            AlterColumn("dbo.RolePermissions", "PermissionId", c => c.String(nullable: false));
            RenameColumn(table: "dbo.RolePermissions", name: "PermissionId", newName: "Permission_Id");
            AddColumn("dbo.RolePermissions", "PermissionId", c => c.String(nullable: false));
            CreateIndex("dbo.RolePermissions", "Role_Id");
            CreateIndex("dbo.RolePermissions", "Permission_Id");
            CreateIndex("dbo.Permissions", "RoleId");
            AddForeignKey("dbo.RolePermissions", "Permission_Id", "dbo.Permissions", "Id");
            AddForeignKey("dbo.RolePermissions", "Role_Id", "dbo.Roles", "Id");
            AddForeignKey("dbo.Permissions", "RoleId", "dbo.Roles", "Id", cascadeDelete: true);
        }
    }
}
