namespace Dextera.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class startModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClientFile = c.String(nullable: false, maxLength: 100),
                        AccountType = c.String(nullable: false, maxLength: 100),
                        AccountTypeName = c.String(),
                        AccountName = c.String(nullable: false, maxLength: 200),
                        AccountLegalName = c.String(maxLength: 200),
                        Address_Street = c.String(maxLength: 200),
                        Address_Unit = c.String(maxLength: 200),
                        City = c.String(maxLength: 100),
                        County = c.String(maxLength: 100),
                        State = c.String(maxLength: 100),
                        StateCode = c.String(maxLength: 10),
                        ZIP = c.String(maxLength: 5),
                        ZIP4 = c.String(maxLength: 4),
                        Phone = c.String(nullable: false, maxLength: 40),
                        PhonePersonName = c.String(maxLength: 100),
                        PhoneExt = c.String(maxLength: 20),
                        cell = c.String(maxLength: 40),
                        CellPersonName = c.String(maxLength: 100),
                        Fax = c.String(maxLength: 40),
                        Email = c.String(nullable: false, maxLength: 60),
                        EmailPersonName = c.String(maxLength: 100),
                        SSN = c.String(maxLength: 20),
                        CostAdvance = c.String(),
                        CostAdvanceAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Retainer = c.String(),
                        RetainerAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AssessmentFee = c.String(),
                        HOAInterest = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AssessmentAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AssessmentsType = c.String(),
                        LateFee = c.String(),
                        LateFeeAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LateFeePercentage = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AssessmentIncrease = c.String(),
                        AssessmentIncreaseAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AssessmentIncreasePercentage = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AssessmentIncreaseDate = c.DateTime(),
                        InterestIncrease = c.String(),
                        InterestIncreasePercentage = c.Decimal(nullable: false, precision: 18, scale: 2),
                        InterestIncreaseDate = c.DateTime(),
                        LateFeeIncrease = c.String(),
                        LateFeeNewIncrease = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LateFeeIncreaseDate = c.DateTime(),
                        SpecialAssessment = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SpecialAssessmentStartDate = c.DateTime(),
                        SpecialAssessmentEndDate = c.DateTime(),
                        SpecialAssessmentType = c.String(),
                        SpecialBilling = c.String(),
                        AttorneyRate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AttorneyRateType = c.String(),
                        CourtFee = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CourtFeeType = c.String(),
                        CostAdvanceFee = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CostAdvanceNotExceed = c.String(),
                        WebSite = c.String(maxLength: 60),
                        TwitterAccount = c.String(maxLength: 60),
                        LinkedIn = c.String(maxLength: 60),
                        CCandRDocument = c.String(maxLength: 100),
                        ContactDocument = c.String(maxLength: 100),
                        ContactSigned = c.DateTime(),
                        ContactAnnual = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        PaymentPlan = c.String(),
                        DownPaymentAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DownPaymentPercentage = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ClientSettlementPercentage = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ClientSettlementType = c.String(),
                        ClientSettlementSpecialRequirement = c.String(),
                        LastBalance = c.String(),
                        TenantId = c.Int(),
                        UpdatedBy = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AccountContacts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AccountID = c.Int(nullable: false),
                        ContactID = c.Int(nullable: false),
                        ContactName = c.String(),
                        ContactAddress = c.String(),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        UpdatedBy = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Accounts", t => t.AccountID, cascadeDelete: true)
                .Index(t => t.AccountID);
            
            CreateTable(
                "dbo.AccountManagementCompanies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AccountID = c.Int(nullable: false),
                        ManagementCompanyID = c.Int(nullable: false),
                        AccountName = c.String(),
                        ManagementCompanyName = c.String(),
                        ManagementCompanyAddress = c.String(),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        UpdatedBy = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Accounts", t => t.AccountID, cascadeDelete: true)
                .ForeignKey("dbo.ManagementCompanies", t => t.ManagementCompanyID, cascadeDelete: true)
                .Index(t => t.AccountID)
                .Index(t => t.ManagementCompanyID);
            
            CreateTable(
                "dbo.AccountPreAuthorizedActions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        accountid = c.Int(nullable: false),
                        preauthorizedactionid = c.Int(nullable: false),
                        preauthrizedactionname = c.String(),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        UpdatedBy = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Accounts", t => t.accountid, cascadeDelete: true)
                .Index(t => t.accountid);
            
            CreateTable(
                "dbo.Contacts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        JobTitle = c.String(maxLength: 40),
                        JobTitleName = c.String(),
                        salutation = c.String(maxLength: 20),
                        firstname = c.String(nullable: false, maxLength: 100),
                        lastname = c.String(maxLength: 100),
                        Physical_Address_Street = c.String(maxLength: 100),
                        Physical_Address_Unit = c.String(maxLength: 100),
                        Physical_City = c.String(maxLength: 40),
                        Physical_County = c.String(maxLength: 40),
                        Physical_State = c.String(maxLength: 40),
                        Physical_StateCode = c.String(maxLength: 4),
                        Physical_ZIP = c.String(maxLength: 20),
                        Physical_ZIP4 = c.String(maxLength: 20),
                        Mailing_Address_Street = c.String(maxLength: 100),
                        Mailing_Address_Unit = c.String(maxLength: 100),
                        Mailing_City = c.String(maxLength: 40),
                        Mailing_County = c.String(maxLength: 40),
                        Mailing_State = c.String(maxLength: 40),
                        Mailing_StateCode = c.String(maxLength: 4),
                        Mailing_ZIP = c.String(maxLength: 20),
                        Mailing_ZIP4 = c.String(maxLength: 20),
                        Phone = c.String(maxLength: 60),
                        PhoneExt = c.String(maxLength: 20),
                        Fax = c.String(maxLength: 40),
                        cell = c.String(maxLength: 100),
                        Email = c.String(maxLength: 20),
                        IsVerify = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        UpdatedBy = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        Account_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Accounts", t => t.Account_Id)
                .Index(t => t.Account_Id);
            
            CreateTable(
                "dbo.Emails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ParentID = c.String(),
                        EmailAddress = c.String(nullable: false, maxLength: 40),
                        PersonName = c.String(maxLength: 100),
                        ParentModule = c.String(),
                        IsPrimary = c.Boolean(nullable: false),
                        IsVerify = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        UpdatedBy = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        Contact_Id = c.Int(),
                        ManagementCompany_Id = c.Int(),
                        Attorney_Id = c.Int(),
                        Bank_Id = c.Int(),
                        Court_CourtID = c.Int(),
                        ProcessServicer_Id = c.Int(),
                        Recorder_Id = c.Int(),
                        Sheriff_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Contacts", t => t.Contact_Id)
                .ForeignKey("dbo.ManagementCompanies", t => t.ManagementCompany_Id)
                .ForeignKey("dbo.Attorneys", t => t.Attorney_Id)
                .ForeignKey("dbo.Banks", t => t.Bank_Id)
                .ForeignKey("dbo.Courts", t => t.Court_CourtID)
                .ForeignKey("dbo.ProcessServicers", t => t.ProcessServicer_Id)
                .ForeignKey("dbo.Recorders", t => t.Recorder_Id)
                .ForeignKey("dbo.Sheriffs", t => t.Sheriff_Id)
                .Index(t => t.Contact_Id)
                .Index(t => t.ManagementCompany_Id)
                .Index(t => t.Attorney_Id)
                .Index(t => t.Bank_Id)
                .Index(t => t.Court_CourtID)
                .Index(t => t.ProcessServicer_Id)
                .Index(t => t.Recorder_Id)
                .Index(t => t.Sheriff_Id);
            
            CreateTable(
                "dbo.ManagerDesignations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        UpdatedBy = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        Contact_Id = c.Int(),
                        ManagementCompany_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Contacts", t => t.Contact_Id)
                .ForeignKey("dbo.ManagementCompanies", t => t.ManagementCompany_Id)
                .Index(t => t.Contact_Id)
                .Index(t => t.ManagementCompany_Id);
            
            CreateTable(
                "dbo.Phones",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ParentID = c.String(),
                        PhoneNumber = c.String(nullable: false, maxLength: 40),
                        PhoneExt = c.String(maxLength: 10),
                        PhonePersonName = c.String(maxLength: 100),
                        ParentModule = c.String(),
                        IsVerify = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        UpdatedBy = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        Contact_Id = c.Int(),
                        ManagementCompany_Id = c.Int(),
                        Attorney_Id = c.Int(),
                        Bank_Id = c.Int(),
                        Court_CourtID = c.Int(),
                        ProcessServicer_Id = c.Int(),
                        Recorder_Id = c.Int(),
                        Sheriff_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Contacts", t => t.Contact_Id)
                .ForeignKey("dbo.ManagementCompanies", t => t.ManagementCompany_Id)
                .ForeignKey("dbo.Attorneys", t => t.Attorney_Id)
                .ForeignKey("dbo.Banks", t => t.Bank_Id)
                .ForeignKey("dbo.Courts", t => t.Court_CourtID)
                .ForeignKey("dbo.ProcessServicers", t => t.ProcessServicer_Id)
                .ForeignKey("dbo.Recorders", t => t.Recorder_Id)
                .ForeignKey("dbo.Sheriffs", t => t.Sheriff_Id)
                .Index(t => t.Contact_Id)
                .Index(t => t.ManagementCompany_Id)
                .Index(t => t.Attorney_Id)
                .Index(t => t.Bank_Id)
                .Index(t => t.Court_CourtID)
                .Index(t => t.ProcessServicer_Id)
                .Index(t => t.Recorder_Id)
                .Index(t => t.Sheriff_Id);
            
            CreateTable(
                "dbo.FeeWaiveds",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        IsSelected = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        UpdatedBy = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        Account_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Accounts", t => t.Account_Id)
                .Index(t => t.Account_Id);
            
            CreateTable(
                "dbo.ManagementCompanies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        LegalName = c.String(nullable: false, maxLength: 100),
                        RoleType = c.String(),
                        RoleTypeName = c.String(),
                        Physical_Address_Street = c.String(maxLength: 100),
                        Physical_Address_Unit = c.String(maxLength: 100),
                        Physical_City = c.String(maxLength: 40),
                        Physical_County = c.String(maxLength: 40),
                        Physical_State = c.String(maxLength: 40),
                        Physical_StateCode = c.String(maxLength: 4),
                        Physical_ZIP = c.String(maxLength: 20),
                        Physical_ZIP4 = c.String(maxLength: 20),
                        Primary_Phone_Number = c.String(nullable: false, maxLength: 40),
                        Primary_Phone_Number_Ext = c.String(maxLength: 10),
                        Primary_PhonePersonName = c.String(maxLength: 100),
                        Primary_Mobile = c.String(maxLength: 40),
                        CellPersonName = c.String(maxLength: 100),
                        Primary_Fax = c.String(maxLength: 20),
                        Primary_Email = c.String(nullable: false, maxLength: 40),
                        Primary_EmailPersonName = c.String(maxLength: 100),
                        Mailing_Address_Street = c.String(maxLength: 100),
                        Mailing_Address_Unit = c.String(maxLength: 100),
                        Mailing_City = c.String(maxLength: 40),
                        Mailing_County = c.String(maxLength: 40),
                        Mailing_State = c.String(maxLength: 40),
                        Mailing_StateCode = c.String(maxLength: 4),
                        Mailing_ZIP = c.String(maxLength: 20),
                        Mailing_ZIP4 = c.String(maxLength: 20),
                        Secondery_Phone_Number = c.String(maxLength: 20),
                        Secondery_Phone_Number_Ext = c.String(maxLength: 10),
                        Secondery_PhonePersonName = c.String(maxLength: 100),
                        Secondery_Mobile = c.String(maxLength: 60),
                        Secondery_MobilePersonName = c.String(maxLength: 100),
                        Secondery_Fax = c.String(maxLength: 20),
                        Secondery_Email = c.String(maxLength: 40),
                        Secondery_EmailPersonName = c.String(maxLength: 100),
                        WebSite = c.String(maxLength: 60),
                        FaceBook = c.String(maxLength: 60),
                        TwitterAccount = c.String(maxLength: 60),
                        LinkedIn = c.String(maxLength: 60),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        UpdatedBy = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ManagCoRoles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        UpdatedBy = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        ManagementCompany_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ManagementCompanies", t => t.ManagementCompany_Id)
                .Index(t => t.ManagementCompany_Id);
            
            CreateTable(
                "dbo.ManagCoManagers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ManagementCompanyId = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 100),
                        Designation = c.String(maxLength: 100),
                        NameDesignation = c.String(maxLength: 100),
                        Phone = c.String(maxLength: 40),
                        PhoneExt = c.String(maxLength: 20),
                        Fax = c.String(maxLength: 40),
                        cell = c.String(maxLength: 100),
                        Email = c.String(maxLength: 20),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        UpdatedBy = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ManagementCompanies", t => t.ManagementCompanyId, cascadeDelete: true)
                .Index(t => t.ManagementCompanyId);
            
            CreateTable(
                "dbo.PreAuthorizedActions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PreAuthorizedActionName = c.String(nullable: false),
                        ParentPreAuthorizedActionId = c.Int(),
                        ParentPreAuthorizedActionName = c.String(),
                        IsSelected = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        UpdatedBy = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        PreAuthorizedAction_Id = c.Int(),
                        Account_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PreAuthorizedActions", t => t.PreAuthorizedAction_Id)
                .ForeignKey("dbo.Accounts", t => t.Account_Id)
                .Index(t => t.PreAuthorizedAction_Id)
                .Index(t => t.Account_Id);
            
            CreateTable(
                "dbo.Attorneys",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Bar = c.String(nullable: false, maxLength: 10),
                        SBN = c.String(nullable: false, maxLength: 12),
                        Phone = c.String(nullable: false, maxLength: 40),
                        PhoneExt = c.String(maxLength: 20),
                        Fax = c.String(maxLength: 40),
                        cell = c.String(maxLength: 40),
                        Email = c.String(nullable: false, maxLength: 60),
                        Facebook = c.String(maxLength: 100),
                        TwitterAccount = c.String(maxLength: 100),
                        LinkedIn = c.String(maxLength: 100),
                        Rate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        UpdatedBy = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Banks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        BranchName = c.String(maxLength: 100),
                        BankDepartment = c.String(maxLength: 100),
                        Address_Street = c.String(maxLength: 200),
                        Address_Unit = c.String(maxLength: 100),
                        County = c.String(maxLength: 100),
                        City = c.String(nullable: false, maxLength: 100),
                        State = c.String(maxLength: 100),
                        StateCode = c.String(maxLength: 4),
                        ZIP = c.String(maxLength: 20),
                        ZIP4 = c.String(maxLength: 10),
                        Phone = c.String(nullable: false, maxLength: 40),
                        PhoneExt = c.String(maxLength: 20),
                        Phone1 = c.String(maxLength: 40),
                        Phone2 = c.String(maxLength: 40),
                        Phone3 = c.String(maxLength: 40),
                        Fax = c.String(maxLength: 40),
                        cell = c.String(maxLength: 40),
                        Email = c.String(maxLength: 60),
                        Email1 = c.String(maxLength: 60),
                        Email2 = c.String(maxLength: 60),
                        Email3 = c.String(maxLength: 60),
                        WebSite = c.String(maxLength: 100),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        UpdatedBy = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Courts",
                c => new
                    {
                        CourtID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        BranchName = c.String(maxLength: 100),
                        Address_Street = c.String(maxLength: 200),
                        Address_Unit = c.String(maxLength: 100),
                        City = c.String(nullable: false, maxLength: 40),
                        County = c.String(maxLength: 100),
                        State = c.String(maxLength: 60),
                        StateCode = c.String(maxLength: 10),
                        District = c.String(maxLength: 100),
                        ZIP = c.String(maxLength: 20),
                        ZIP4 = c.String(maxLength: 10),
                        Phone = c.String(maxLength: 40),
                        PhoneExt = c.String(maxLength: 20),
                        Fax = c.String(maxLength: 40),
                        cell = c.String(maxLength: 40),
                        Email = c.String(maxLength: 60),
                        WebSite = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.CourtID);
            
            CreateTable(
                "dbo.MenuModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Action = c.String(),
                        Controller = c.String(),
                        ParentId = c.Int(nullable: false),
                        SortOrder = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        UpdatedBy = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        UpdatedBy = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProcessServicers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        BranchName = c.String(maxLength: 100),
                        Address_Street = c.String(maxLength: 200),
                        Address_Unit = c.String(maxLength: 100),
                        City = c.String(nullable: false, maxLength: 100),
                        County = c.String(maxLength: 100),
                        State = c.String(maxLength: 100),
                        StateCode = c.String(maxLength: 10),
                        ZIP = c.String(maxLength: 20),
                        ZIP4 = c.String(maxLength: 20),
                        Phone = c.String(nullable: false, maxLength: 40),
                        PhoneExt = c.String(maxLength: 20),
                        PhonePersonName = c.String(maxLength: 100),
                        Fax = c.String(maxLength: 40),
                        cell = c.String(maxLength: 40),
                        CellPersonName = c.String(maxLength: 100),
                        Email = c.String(nullable: false, maxLength: 60),
                        EmailPersonName = c.String(maxLength: 100),
                        WebSite = c.String(maxLength: 100),
                        Facebook = c.String(maxLength: 100),
                        TwitterAccount = c.String(maxLength: 100),
                        LinkedIn = c.String(maxLength: 100),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        UpdatedBy = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Recorders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        BranchName = c.String(maxLength: 100),
                        Address_Street = c.String(maxLength: 200),
                        Address_Unit = c.String(maxLength: 100),
                        City = c.String(maxLength: 40),
                        District = c.String(maxLength: 100),
                        County = c.String(maxLength: 100),
                        State = c.String(maxLength: 100),
                        StateCode = c.String(maxLength: 10),
                        ZIP = c.String(maxLength: 20),
                        ZIP4 = c.String(maxLength: 10),
                        Phone = c.String(maxLength: 40),
                        PhoneExt = c.String(maxLength: 20),
                        PhonePersonName = c.String(maxLength: 100),
                        Fax = c.String(maxLength: 40),
                        cell = c.String(maxLength: 40),
                        cellPersonName = c.String(maxLength: 100),
                        Email = c.String(maxLength: 60),
                        EmailPersonName = c.String(maxLength: 100),
                        WebSite = c.String(maxLength: 100),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        UpdatedBy = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Sheriffs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        BranchName = c.String(maxLength: 100),
                        Address_Street = c.String(maxLength: 200),
                        Address_Unit = c.String(maxLength: 100),
                        City = c.String(maxLength: 40),
                        County = c.String(maxLength: 100),
                        State = c.String(maxLength: 100),
                        StateCode = c.String(maxLength: 10),
                        ZIP = c.String(maxLength: 20),
                        ZIP4 = c.String(maxLength: 20),
                        Phone = c.String(maxLength: 40),
                        PhoneExt = c.String(maxLength: 20),
                        PhonePersonName = c.String(maxLength: 100),
                        Fax = c.String(maxLength: 40),
                        cell = c.String(maxLength: 40),
                        cellPersonName = c.String(maxLength: 100),
                        Email = c.String(maxLength: 40),
                        EmailPersonName = c.String(maxLength: 100),
                        WebSite = c.String(maxLength: 100),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        UpdatedBy = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Hometown = c.String(),
                        TenantId = c.Int(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Phone = c.String(maxLength: 40),
                        PhoneExt = c.String(maxLength: 20),
                        salt = c.String(maxLength: 4),
                        useractive = c.Boolean(nullable: false),
                        resetpassword = c.Boolean(nullable: false),
                        Alias = c.String(maxLength: 50),
                        Bar = c.String(maxLength: 40),
                        IM = c.String(maxLength: 40),
                        FullName = c.String(),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.ManagementCompanyAccounts",
                c => new
                    {
                        ManagementCompany_Id = c.Int(nullable: false),
                        Account_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ManagementCompany_Id, t.Account_Id })
                .ForeignKey("dbo.ManagementCompanies", t => t.ManagementCompany_Id, cascadeDelete: true)
                .ForeignKey("dbo.Accounts", t => t.Account_Id, cascadeDelete: true)
                .Index(t => t.ManagementCompany_Id)
                .Index(t => t.Account_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Phones", "Sheriff_Id", "dbo.Sheriffs");
            DropForeignKey("dbo.Emails", "Sheriff_Id", "dbo.Sheriffs");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Phones", "Recorder_Id", "dbo.Recorders");
            DropForeignKey("dbo.Emails", "Recorder_Id", "dbo.Recorders");
            DropForeignKey("dbo.Phones", "ProcessServicer_Id", "dbo.ProcessServicers");
            DropForeignKey("dbo.Emails", "ProcessServicer_Id", "dbo.ProcessServicers");
            DropForeignKey("dbo.Phones", "Court_CourtID", "dbo.Courts");
            DropForeignKey("dbo.Emails", "Court_CourtID", "dbo.Courts");
            DropForeignKey("dbo.Phones", "Bank_Id", "dbo.Banks");
            DropForeignKey("dbo.Emails", "Bank_Id", "dbo.Banks");
            DropForeignKey("dbo.Phones", "Attorney_Id", "dbo.Attorneys");
            DropForeignKey("dbo.Emails", "Attorney_Id", "dbo.Attorneys");
            DropForeignKey("dbo.PreAuthorizedActions", "Account_Id", "dbo.Accounts");
            DropForeignKey("dbo.PreAuthorizedActions", "PreAuthorizedAction_Id", "dbo.PreAuthorizedActions");
            DropForeignKey("dbo.Phones", "ManagementCompany_Id", "dbo.ManagementCompanies");
            DropForeignKey("dbo.Emails", "ManagementCompany_Id", "dbo.ManagementCompanies");
            DropForeignKey("dbo.ManagCoManagers", "ManagementCompanyId", "dbo.ManagementCompanies");
            DropForeignKey("dbo.ManagCoRoles", "ManagementCompany_Id", "dbo.ManagementCompanies");
            DropForeignKey("dbo.ManagerDesignations", "ManagementCompany_Id", "dbo.ManagementCompanies");
            DropForeignKey("dbo.ManagementCompanyAccounts", "Account_Id", "dbo.Accounts");
            DropForeignKey("dbo.ManagementCompanyAccounts", "ManagementCompany_Id", "dbo.ManagementCompanies");
            DropForeignKey("dbo.AccountManagementCompanies", "ManagementCompanyID", "dbo.ManagementCompanies");
            DropForeignKey("dbo.FeeWaiveds", "Account_Id", "dbo.Accounts");
            DropForeignKey("dbo.Contacts", "Account_Id", "dbo.Accounts");
            DropForeignKey("dbo.Phones", "Contact_Id", "dbo.Contacts");
            DropForeignKey("dbo.ManagerDesignations", "Contact_Id", "dbo.Contacts");
            DropForeignKey("dbo.Emails", "Contact_Id", "dbo.Contacts");
            DropForeignKey("dbo.AccountPreAuthorizedActions", "accountid", "dbo.Accounts");
            DropForeignKey("dbo.AccountManagementCompanies", "AccountID", "dbo.Accounts");
            DropForeignKey("dbo.AccountContacts", "AccountID", "dbo.Accounts");
            DropIndex("dbo.ManagementCompanyAccounts", new[] { "Account_Id" });
            DropIndex("dbo.ManagementCompanyAccounts", new[] { "ManagementCompany_Id" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.PreAuthorizedActions", new[] { "Account_Id" });
            DropIndex("dbo.PreAuthorizedActions", new[] { "PreAuthorizedAction_Id" });
            DropIndex("dbo.ManagCoManagers", new[] { "ManagementCompanyId" });
            DropIndex("dbo.ManagCoRoles", new[] { "ManagementCompany_Id" });
            DropIndex("dbo.FeeWaiveds", new[] { "Account_Id" });
            DropIndex("dbo.Phones", new[] { "Sheriff_Id" });
            DropIndex("dbo.Phones", new[] { "Recorder_Id" });
            DropIndex("dbo.Phones", new[] { "ProcessServicer_Id" });
            DropIndex("dbo.Phones", new[] { "Court_CourtID" });
            DropIndex("dbo.Phones", new[] { "Bank_Id" });
            DropIndex("dbo.Phones", new[] { "Attorney_Id" });
            DropIndex("dbo.Phones", new[] { "ManagementCompany_Id" });
            DropIndex("dbo.Phones", new[] { "Contact_Id" });
            DropIndex("dbo.ManagerDesignations", new[] { "ManagementCompany_Id" });
            DropIndex("dbo.ManagerDesignations", new[] { "Contact_Id" });
            DropIndex("dbo.Emails", new[] { "Sheriff_Id" });
            DropIndex("dbo.Emails", new[] { "Recorder_Id" });
            DropIndex("dbo.Emails", new[] { "ProcessServicer_Id" });
            DropIndex("dbo.Emails", new[] { "Court_CourtID" });
            DropIndex("dbo.Emails", new[] { "Bank_Id" });
            DropIndex("dbo.Emails", new[] { "Attorney_Id" });
            DropIndex("dbo.Emails", new[] { "ManagementCompany_Id" });
            DropIndex("dbo.Emails", new[] { "Contact_Id" });
            DropIndex("dbo.Contacts", new[] { "Account_Id" });
            DropIndex("dbo.AccountPreAuthorizedActions", new[] { "accountid" });
            DropIndex("dbo.AccountManagementCompanies", new[] { "ManagementCompanyID" });
            DropIndex("dbo.AccountManagementCompanies", new[] { "AccountID" });
            DropIndex("dbo.AccountContacts", new[] { "AccountID" });
            DropTable("dbo.ManagementCompanyAccounts");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Sheriffs");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Recorders");
            DropTable("dbo.ProcessServicers");
            DropTable("dbo.Roles");
            DropTable("dbo.MenuModels");
            DropTable("dbo.Courts");
            DropTable("dbo.Banks");
            DropTable("dbo.Attorneys");
            DropTable("dbo.PreAuthorizedActions");
            DropTable("dbo.ManagCoManagers");
            DropTable("dbo.ManagCoRoles");
            DropTable("dbo.ManagementCompanies");
            DropTable("dbo.FeeWaiveds");
            DropTable("dbo.Phones");
            DropTable("dbo.ManagerDesignations");
            DropTable("dbo.Emails");
            DropTable("dbo.Contacts");
            DropTable("dbo.AccountPreAuthorizedActions");
            DropTable("dbo.AccountManagementCompanies");
            DropTable("dbo.AccountContacts");
            DropTable("dbo.Accounts");
        }
    }
}
