namespace Dextera.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class client : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Assessments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Cases",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AddressStreet = c.String(),
                        AddressUnit = c.String(),
                        AddressCity = c.String(),
                        AddressState = c.String(),
                        AddressZip = c.String(),
                        AddressPhone = c.String(),
                        ClientId = c.Int(),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clients", t => t.ClientId)
                .Index(t => t.ClientId);
            
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClientName = c.String(),
                        ClientType = c.String(),
                        ClientAddress = c.String(),
                        CityStateZip = c.String(),
                        GeneralPhone = c.String(),
                        Fax = c.String(),
                        Website = c.String(),
                        LegalName = c.String(),
                        Attorney = c.Int(),
                        Annual = c.Boolean(nullable: false),
                        SubClientId = c.Int(),
                        Signed = c.DateTime(),
                        Address = c.String(),
                        CityStateZip4Second = c.String(),
                        PhoneExt = c.String(),
                        Mobile = c.String(),
                        EmailContact = c.String(),
                        RoleId = c.Int(),
                        CostAdvanceAmountMoney = c.String(),
                        CostAdvanceRB = c.Boolean(nullable: false),
                        RetainerAmountMoney = c.String(),
                        RetainerRB = c.Boolean(nullable: false),
                        AssessmentFeesRB = c.Boolean(nullable: false),
                        AssessmentMoney = c.String(),
                        AssessmentId = c.Int(),
                        HOAInterestPercent = c.String(),
                        LateFeeMoney = c.String(),
                        LateFeePercent = c.String(),
                        AssessmentIncMoney = c.String(),
                        AssessmentIncPercent = c.String(),
                        InterestIncreaseRB = c.Boolean(nullable: false),
                        InterestIncreasePercent = c.String(),
                        LateFeeIncreaseRB = c.Boolean(nullable: false),
                        LateFeeIncreaseMoney = c.String(),
                        IncreaseStartDate = c.DateTime(),
                        SpecialAssessmentMoney = c.String(),
                        SpecialAssessmentStartDate = c.DateTime(),
                        SpecialAssessmentEndDate = c.DateTime(),
                        SpecialAssessmentId = c.Int(),
                        SpecialBillingRB = c.Boolean(nullable: false),
                        AttorneyRateMoney = c.String(),
                        AttorneyRateId = c.Int(),
                        CourtFeesMoney = c.String(),
                        CourtFeesId = c.Int(),
                        CostAdvanceFeesMoney = c.String(),
                        CostAdvanceFeesId = c.Int(),
                        LengthPaymentId = c.Int(),
                        DownPaymentMoney = c.String(),
                        DownPaymentPercent = c.String(),
                        FeesThatId = c.Int(),
                        WillSettlePercent = c.String(),
                        WillSettleId = c.Int(),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Assessments", t => t.AssessmentId)
                .ForeignKey("dbo.RatePeriods", t => t.AttorneyRateId)
                .ForeignKey("dbo.CostAdvanceFees", t => t.CostAdvanceFeesId)
                .ForeignKey("dbo.RatePeriods", t => t.CourtFeesId)
                .ForeignKey("dbo.FeesThats", t => t.FeesThatId)
                .ForeignKey("dbo.LengthPayments", t => t.LengthPaymentId)
                .ForeignKey("dbo.Roles", t => t.RoleId)
                .ForeignKey("dbo.Assessments", t => t.SpecialAssessmentId)
                .ForeignKey("dbo.Clients", t => t.SubClientId)
                .ForeignKey("dbo.WillSettles", t => t.WillSettleId)
                .Index(t => t.SubClientId)
                .Index(t => t.RoleId)
                .Index(t => t.AssessmentId)
                .Index(t => t.SpecialAssessmentId)
                .Index(t => t.AttorneyRateId)
                .Index(t => t.CourtFeesId)
                .Index(t => t.CostAdvanceFeesId)
                .Index(t => t.LengthPaymentId)
                .Index(t => t.FeesThatId)
                .Index(t => t.WillSettleId);
            
            CreateTable(
                "dbo.RatePeriods",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AttorneyRateName = c.String(),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CostAdvanceFees",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CostAdvanceFeesName = c.String(),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FeesThats",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FeesThatName = c.String(),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.LengthPayments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LengthPaymentName = c.String(),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.WillSettles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        WillSettleName = c.String(),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Cases", "ClientId", "dbo.Clients");
            DropForeignKey("dbo.Clients", "WillSettleId", "dbo.WillSettles");
            DropForeignKey("dbo.Clients", "SubClientId", "dbo.Clients");
            DropForeignKey("dbo.Clients", "SpecialAssessmentId", "dbo.Assessments");
            DropForeignKey("dbo.Clients", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.Clients", "LengthPaymentId", "dbo.LengthPayments");
            DropForeignKey("dbo.Clients", "FeesThatId", "dbo.FeesThats");
            DropForeignKey("dbo.Clients", "CourtFeesId", "dbo.RatePeriods");
            DropForeignKey("dbo.Clients", "CostAdvanceFeesId", "dbo.CostAdvanceFees");
            DropForeignKey("dbo.Clients", "AttorneyRateId", "dbo.RatePeriods");
            DropForeignKey("dbo.Clients", "AssessmentId", "dbo.Assessments");
            DropIndex("dbo.Clients", new[] { "WillSettleId" });
            DropIndex("dbo.Clients", new[] { "FeesThatId" });
            DropIndex("dbo.Clients", new[] { "LengthPaymentId" });
            DropIndex("dbo.Clients", new[] { "CostAdvanceFeesId" });
            DropIndex("dbo.Clients", new[] { "CourtFeesId" });
            DropIndex("dbo.Clients", new[] { "AttorneyRateId" });
            DropIndex("dbo.Clients", new[] { "SpecialAssessmentId" });
            DropIndex("dbo.Clients", new[] { "AssessmentId" });
            DropIndex("dbo.Clients", new[] { "RoleId" });
            DropIndex("dbo.Clients", new[] { "SubClientId" });
            DropIndex("dbo.Cases", new[] { "ClientId" });
            DropTable("dbo.WillSettles");
            DropTable("dbo.LengthPayments");
            DropTable("dbo.FeesThats");
            DropTable("dbo.CostAdvanceFees");
            DropTable("dbo.RatePeriods");
            DropTable("dbo.Clients");
            DropTable("dbo.Cases");
            DropTable("dbo.Assessments");
        }
    }
}
