namespace Dextera.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class defenant : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cases", "AdvanceRetainerReceived", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Cases", "AdvanceRetainerReceived");
        }
    }
}
