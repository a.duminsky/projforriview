namespace Dextera.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeleteOldAccut : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AccountContacts", "AccountID", "dbo.Accounts");
            DropForeignKey("dbo.AccountManagementCompanies", "AccountID", "dbo.Accounts");
            DropForeignKey("dbo.AccountPreAuthorizedActions", "accountid", "dbo.Accounts");
            DropForeignKey("dbo.Contacts", "Account_Id", "dbo.Accounts");
            DropForeignKey("dbo.FeeWaiveds", "Account_Id", "dbo.Accounts");
            DropForeignKey("dbo.AccountManagementCompanies", "ManagementCompanyID", "dbo.ManagementCompanies");
            DropForeignKey("dbo.ManagementCompanyAccounts", "ManagementCompany_Id", "dbo.ManagementCompanies");
            DropForeignKey("dbo.ManagementCompanyAccounts", "Account_Id", "dbo.Accounts");
            DropForeignKey("dbo.ManagerDesignations", "ManagementCompany_Id", "dbo.ManagementCompanies");
            DropForeignKey("dbo.ManagCoRoles", "ManagementCompany_Id", "dbo.ManagementCompanies");
            DropForeignKey("dbo.ManagCoManagers", "ManagementCompanyId", "dbo.ManagementCompanies");
            DropForeignKey("dbo.Emails", "ManagementCompany_Id", "dbo.ManagementCompanies");
            DropForeignKey("dbo.Phones", "ManagementCompany_Id", "dbo.ManagementCompanies");
            DropForeignKey("dbo.PreAuthorizedActions", "PreAuthorizedAction_Id", "dbo.PreAuthorizedActions");
            DropForeignKey("dbo.PreAuthorizedActions", "Account_Id", "dbo.Accounts");
            DropIndex("dbo.AccountContacts", new[] { "AccountID" });
            DropIndex("dbo.AccountManagementCompanies", new[] { "AccountID" });
            DropIndex("dbo.AccountManagementCompanies", new[] { "ManagementCompanyID" });
            DropIndex("dbo.AccountPreAuthorizedActions", new[] { "accountid" });
            DropIndex("dbo.Contacts", new[] { "Account_Id" });
            DropIndex("dbo.Emails", new[] { "ManagementCompany_Id" });
            DropIndex("dbo.ManagerDesignations", new[] { "ManagementCompany_Id" });
            DropIndex("dbo.Phones", new[] { "ManagementCompany_Id" });
            DropIndex("dbo.FeeWaiveds", new[] { "Account_Id" });
            DropIndex("dbo.ManagCoRoles", new[] { "ManagementCompany_Id" });
            DropIndex("dbo.ManagCoManagers", new[] { "ManagementCompanyId" });
            DropIndex("dbo.PreAuthorizedActions", new[] { "PreAuthorizedAction_Id" });
            DropIndex("dbo.PreAuthorizedActions", new[] { "Account_Id" });
            DropIndex("dbo.ManagementCompanyAccounts", new[] { "ManagementCompany_Id" });
            DropIndex("dbo.ManagementCompanyAccounts", new[] { "Account_Id" });
            DropColumn("dbo.Contacts", "Account_Id");
            DropColumn("dbo.Emails", "ManagementCompany_Id");
            DropColumn("dbo.ManagerDesignations", "ManagementCompany_Id");
            DropColumn("dbo.Phones", "ManagementCompany_Id");
            DropTable("dbo.Accounts");
            DropTable("dbo.AccountContacts");
            DropTable("dbo.AccountManagementCompanies");
            DropTable("dbo.AccountPreAuthorizedActions");
            DropTable("dbo.FeeWaiveds");
            DropTable("dbo.ManagementCompanies");
            DropTable("dbo.ManagCoRoles");
            DropTable("dbo.ManagCoManagers");
            DropTable("dbo.PreAuthorizedActions");
            DropTable("dbo.ManagementCompanyAccounts");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ManagementCompanyAccounts",
                c => new
                    {
                        ManagementCompany_Id = c.Int(nullable: false),
                        Account_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ManagementCompany_Id, t.Account_Id });
            
            CreateTable(
                "dbo.PreAuthorizedActions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PreAuthorizedActionName = c.String(nullable: false),
                        ParentPreAuthorizedActionId = c.Int(),
                        ParentPreAuthorizedActionName = c.String(),
                        IsSelected = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        PreAuthorizedAction_Id = c.Int(),
                        Account_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ManagCoManagers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ManagementCompanyId = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 100),
                        Designation = c.String(maxLength: 100),
                        NameDesignation = c.String(maxLength: 100),
                        Phone = c.String(maxLength: 40),
                        PhoneExt = c.String(maxLength: 20),
                        Fax = c.String(maxLength: 40),
                        cell = c.String(maxLength: 100),
                        Email = c.String(maxLength: 20),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ManagCoRoles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        ManagementCompany_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ManagementCompanies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        LegalName = c.String(nullable: false, maxLength: 100),
                        RoleType = c.String(),
                        RoleTypeName = c.String(),
                        Physical_Address_Street = c.String(maxLength: 100),
                        Physical_Address_Unit = c.String(maxLength: 100),
                        Physical_City = c.String(maxLength: 40),
                        Physical_County = c.String(maxLength: 40),
                        Physical_State = c.String(maxLength: 40),
                        Physical_StateCode = c.String(maxLength: 4),
                        Physical_ZIP = c.String(maxLength: 20),
                        Physical_ZIP4 = c.String(maxLength: 20),
                        Primary_Phone_Number = c.String(nullable: false, maxLength: 40),
                        Primary_Phone_Number_Ext = c.String(maxLength: 10),
                        Primary_PhonePersonName = c.String(maxLength: 100),
                        Primary_Mobile = c.String(maxLength: 40),
                        CellPersonName = c.String(maxLength: 100),
                        Primary_Fax = c.String(maxLength: 20),
                        Primary_Email = c.String(nullable: false, maxLength: 40),
                        Primary_EmailPersonName = c.String(maxLength: 100),
                        Mailing_Address_Street = c.String(maxLength: 100),
                        Mailing_Address_Unit = c.String(maxLength: 100),
                        Mailing_City = c.String(maxLength: 40),
                        Mailing_County = c.String(maxLength: 40),
                        Mailing_State = c.String(maxLength: 40),
                        Mailing_StateCode = c.String(maxLength: 4),
                        Mailing_ZIP = c.String(maxLength: 20),
                        Mailing_ZIP4 = c.String(maxLength: 20),
                        Secondery_Phone_Number = c.String(maxLength: 20),
                        Secondery_Phone_Number_Ext = c.String(maxLength: 10),
                        Secondery_PhonePersonName = c.String(maxLength: 100),
                        Secondery_Mobile = c.String(maxLength: 60),
                        Secondery_MobilePersonName = c.String(maxLength: 100),
                        Secondery_Fax = c.String(maxLength: 20),
                        Secondery_Email = c.String(maxLength: 40),
                        Secondery_EmailPersonName = c.String(maxLength: 100),
                        WebSite = c.String(maxLength: 60),
                        FaceBook = c.String(maxLength: 60),
                        TwitterAccount = c.String(maxLength: 60),
                        LinkedIn = c.String(maxLength: 60),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FeeWaiveds",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        IsSelected = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        Account_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AccountPreAuthorizedActions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        accountid = c.Int(nullable: false),
                        preauthorizedactionid = c.Int(nullable: false),
                        preauthrizedactionname = c.String(),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AccountManagementCompanies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AccountID = c.Int(nullable: false),
                        ManagementCompanyID = c.Int(nullable: false),
                        AccountName = c.String(),
                        ManagementCompanyName = c.String(),
                        ManagementCompanyAddress = c.String(),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AccountContacts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AccountID = c.Int(nullable: false),
                        ContactID = c.Int(nullable: false),
                        ContactName = c.String(),
                        ContactAddress = c.String(),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClientFile = c.String(nullable: false, maxLength: 100),
                        AccountType = c.String(nullable: false, maxLength: 100),
                        AccountTypeName = c.String(),
                        AccountName = c.String(nullable: false, maxLength: 200),
                        AccountLegalName = c.String(maxLength: 200),
                        Address_Street = c.String(maxLength: 200),
                        Address_Unit = c.String(maxLength: 200),
                        City = c.String(maxLength: 100),
                        County = c.String(maxLength: 100),
                        State = c.String(maxLength: 100),
                        StateCode = c.String(maxLength: 10),
                        ZIP = c.String(maxLength: 5),
                        ZIP4 = c.String(maxLength: 4),
                        Phone = c.String(nullable: false, maxLength: 40),
                        PhonePersonName = c.String(maxLength: 100),
                        PhoneExt = c.String(maxLength: 20),
                        cell = c.String(maxLength: 40),
                        CellPersonName = c.String(maxLength: 100),
                        Fax = c.String(maxLength: 40),
                        Email = c.String(nullable: false, maxLength: 60),
                        EmailPersonName = c.String(maxLength: 100),
                        SSN = c.String(maxLength: 20),
                        CostAdvance = c.String(),
                        CostAdvanceAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Retainer = c.String(),
                        RetainerAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AssessmentFee = c.String(),
                        HOAInterest = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AssessmentAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AssessmentsType = c.String(),
                        LateFee = c.String(),
                        LateFeeAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LateFeePercentage = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AssessmentIncrease = c.String(),
                        AssessmentIncreaseAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AssessmentIncreasePercentage = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AssessmentIncreaseDate = c.DateTime(),
                        InterestIncrease = c.String(),
                        InterestIncreasePercentage = c.Decimal(nullable: false, precision: 18, scale: 2),
                        InterestIncreaseDate = c.DateTime(),
                        LateFeeIncrease = c.String(),
                        LateFeeNewIncrease = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LateFeeIncreaseDate = c.DateTime(),
                        SpecialAssessment = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SpecialAssessmentStartDate = c.DateTime(),
                        SpecialAssessmentEndDate = c.DateTime(),
                        SpecialAssessmentType = c.String(),
                        SpecialBilling = c.String(),
                        AttorneyRate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AttorneyRateType = c.String(),
                        CourtFee = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CourtFeeType = c.String(),
                        CostAdvanceFee = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CostAdvanceNotExceed = c.String(),
                        WebSite = c.String(maxLength: 60),
                        TwitterAccount = c.String(maxLength: 60),
                        LinkedIn = c.String(maxLength: 60),
                        CCandRDocument = c.String(maxLength: 100),
                        ContactDocument = c.String(maxLength: 100),
                        ContactSigned = c.DateTime(),
                        ContactAnnual = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        PaymentPlan = c.String(),
                        DownPaymentAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DownPaymentPercentage = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ClientSettlementPercentage = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ClientSettlementType = c.String(),
                        ClientSettlementSpecialRequirement = c.String(),
                        LastBalance = c.String(),
                        TenantId = c.Int(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Phones", "ManagementCompany_Id", c => c.Int());
            AddColumn("dbo.ManagerDesignations", "ManagementCompany_Id", c => c.Int());
            AddColumn("dbo.Emails", "ManagementCompany_Id", c => c.Int());
            AddColumn("dbo.Contacts", "Account_Id", c => c.Int());
            CreateIndex("dbo.ManagementCompanyAccounts", "Account_Id");
            CreateIndex("dbo.ManagementCompanyAccounts", "ManagementCompany_Id");
            CreateIndex("dbo.PreAuthorizedActions", "Account_Id");
            CreateIndex("dbo.PreAuthorizedActions", "PreAuthorizedAction_Id");
            CreateIndex("dbo.ManagCoManagers", "ManagementCompanyId");
            CreateIndex("dbo.ManagCoRoles", "ManagementCompany_Id");
            CreateIndex("dbo.FeeWaiveds", "Account_Id");
            CreateIndex("dbo.Phones", "ManagementCompany_Id");
            CreateIndex("dbo.ManagerDesignations", "ManagementCompany_Id");
            CreateIndex("dbo.Emails", "ManagementCompany_Id");
            CreateIndex("dbo.Contacts", "Account_Id");
            CreateIndex("dbo.AccountPreAuthorizedActions", "accountid");
            CreateIndex("dbo.AccountManagementCompanies", "ManagementCompanyID");
            CreateIndex("dbo.AccountManagementCompanies", "AccountID");
            CreateIndex("dbo.AccountContacts", "AccountID");
            AddForeignKey("dbo.PreAuthorizedActions", "Account_Id", "dbo.Accounts", "Id");
            AddForeignKey("dbo.PreAuthorizedActions", "PreAuthorizedAction_Id", "dbo.PreAuthorizedActions", "Id");
            AddForeignKey("dbo.Phones", "ManagementCompany_Id", "dbo.ManagementCompanies", "Id");
            AddForeignKey("dbo.Emails", "ManagementCompany_Id", "dbo.ManagementCompanies", "Id");
            AddForeignKey("dbo.ManagCoManagers", "ManagementCompanyId", "dbo.ManagementCompanies", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ManagCoRoles", "ManagementCompany_Id", "dbo.ManagementCompanies", "Id");
            AddForeignKey("dbo.ManagerDesignations", "ManagementCompany_Id", "dbo.ManagementCompanies", "Id");
            AddForeignKey("dbo.ManagementCompanyAccounts", "Account_Id", "dbo.Accounts", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ManagementCompanyAccounts", "ManagementCompany_Id", "dbo.ManagementCompanies", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AccountManagementCompanies", "ManagementCompanyID", "dbo.ManagementCompanies", "Id", cascadeDelete: true);
            AddForeignKey("dbo.FeeWaiveds", "Account_Id", "dbo.Accounts", "Id");
            AddForeignKey("dbo.Contacts", "Account_Id", "dbo.Accounts", "Id");
            AddForeignKey("dbo.AccountPreAuthorizedActions", "accountid", "dbo.Accounts", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AccountManagementCompanies", "AccountID", "dbo.Accounts", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AccountContacts", "AccountID", "dbo.Accounts", "Id", cascadeDelete: true);
        }
    }
}
