namespace Dextera.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tenants : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tenants",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClientName = c.String(),
                        ClientAddress = c.String(),
                        ClientCity = c.String(),
                        ClientState = c.String(),
                        ClientZip = c.String(),
                        ClientPhone = c.String(),
                        ClientFax = c.String(),
                        ClientWebSite = c.String(),
                        SubClientName = c.String(),
                        SubClientAddress = c.String(),
                        SubClientCity = c.String(),
                        SubClientState = c.String(),
                        SubClientZip = c.String(),
                        SubClientPhone = c.String(),
                        SubClientFax = c.String(),
                        SubClientWebSite = c.String(),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Tenants");
        }
    }
}
