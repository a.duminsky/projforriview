// <auto-generated />
namespace Dextera.Dal.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class changeCreatedByAndUpdateBy : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(changeCreatedByAndUpdateBy));
        
        string IMigrationMetadata.Id
        {
            get { return "201709221754560_changeCreatedByAndUpdateBy"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
