namespace Dextera.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newFieldForTenant : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tenants", "ClientCountry", c => c.String());
            AddColumn("dbo.Tenants", "SubClientCountry", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tenants", "SubClientCountry");
            DropColumn("dbo.Tenants", "ClientCountry");
        }
    }
}
