namespace Dextera.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fixTypo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Defenants", "Height", c => c.Double());
            DropColumn("dbo.Defenants", "Heihgt");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Defenants", "Heihgt", c => c.Double());
            DropColumn("dbo.Defenants", "Height");
        }
    }
}
