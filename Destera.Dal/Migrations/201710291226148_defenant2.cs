namespace Dextera.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class defenant2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Defenants",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClientId = c.Int(nullable: false),
                        FirstName = c.String(),
                        MiddleName = c.String(),
                        LastName = c.String(),
                        GenderId = c.Int(),
                        Heihgt = c.Double(),
                        HairId = c.Int(),
                        EyesId = c.Int(),
                        EthnicId = c.Int(),
                        AddressStreet = c.String(),
                        AddressUnit = c.String(),
                        AddressCity = c.String(),
                        AddressState = c.String(),
                        AddressZip = c.String(),
                        AddressPhone = c.String(),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        Case_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Ethnics", t => t.EthnicId)
                .ForeignKey("dbo.Eyes", t => t.EyesId)
                .ForeignKey("dbo.Genders", t => t.GenderId)
                .ForeignKey("dbo.Hair", t => t.HairId)
                .ForeignKey("dbo.Cases", t => t.Case_Id)
                .Index(t => t.GenderId)
                .Index(t => t.HairId)
                .Index(t => t.EyesId)
                .Index(t => t.EthnicId)
                .Index(t => t.Case_Id);
            
            CreateTable(
                "dbo.Ethnics",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Eyes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Genders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Hair",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Defenants", "Case_Id", "dbo.Cases");
            DropForeignKey("dbo.Defenants", "HairId", "dbo.Hair");
            DropForeignKey("dbo.Defenants", "GenderId", "dbo.Genders");
            DropForeignKey("dbo.Defenants", "EyesId", "dbo.Eyes");
            DropForeignKey("dbo.Defenants", "EthnicId", "dbo.Ethnics");
            DropIndex("dbo.Defenants", new[] { "Case_Id" });
            DropIndex("dbo.Defenants", new[] { "EthnicId" });
            DropIndex("dbo.Defenants", new[] { "EyesId" });
            DropIndex("dbo.Defenants", new[] { "HairId" });
            DropIndex("dbo.Defenants", new[] { "GenderId" });
            DropTable("dbo.Hair");
            DropTable("dbo.Genders");
            DropTable("dbo.Eyes");
            DropTable("dbo.Ethnics");
            DropTable("dbo.Defenants");
        }
    }
}
