namespace Dextera.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeObsolete : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Emails", "Attorney_Id", "dbo.Attorneys");
            DropForeignKey("dbo.Phones", "Attorney_Id", "dbo.Attorneys");
            DropForeignKey("dbo.Emails", "Bank_Id", "dbo.Banks");
            DropForeignKey("dbo.Phones", "Bank_Id", "dbo.Banks");
            DropForeignKey("dbo.Emails", "Contact_Id", "dbo.Contacts");
            DropForeignKey("dbo.ManagerDesignations", "Contact_Id", "dbo.Contacts");
            DropForeignKey("dbo.Phones", "Contact_Id", "dbo.Contacts");
            DropForeignKey("dbo.Emails", "Court_CourtID", "dbo.Courts");
            DropForeignKey("dbo.Phones", "Court_CourtID", "dbo.Courts");
            DropForeignKey("dbo.Emails", "ProcessServicer_Id", "dbo.ProcessServicers");
            DropForeignKey("dbo.Phones", "ProcessServicer_Id", "dbo.ProcessServicers");
            DropForeignKey("dbo.Emails", "Recorder_Id", "dbo.Recorders");
            DropForeignKey("dbo.Phones", "Recorder_Id", "dbo.Recorders");
            DropForeignKey("dbo.Emails", "Sheriff_Id", "dbo.Sheriffs");
            DropForeignKey("dbo.Phones", "Sheriff_Id", "dbo.Sheriffs");
            DropIndex("dbo.Emails", new[] { "Attorney_Id" });
            DropIndex("dbo.Emails", new[] { "Bank_Id" });
            DropIndex("dbo.Emails", new[] { "Contact_Id" });
            DropIndex("dbo.Emails", new[] { "Court_CourtID" });
            DropIndex("dbo.Emails", new[] { "ProcessServicer_Id" });
            DropIndex("dbo.Emails", new[] { "Recorder_Id" });
            DropIndex("dbo.Emails", new[] { "Sheriff_Id" });
            DropIndex("dbo.Phones", new[] { "Attorney_Id" });
            DropIndex("dbo.Phones", new[] { "Bank_Id" });
            DropIndex("dbo.Phones", new[] { "Contact_Id" });
            DropIndex("dbo.Phones", new[] { "Court_CourtID" });
            DropIndex("dbo.Phones", new[] { "ProcessServicer_Id" });
            DropIndex("dbo.Phones", new[] { "Recorder_Id" });
            DropIndex("dbo.Phones", new[] { "Sheriff_Id" });
            DropIndex("dbo.ManagerDesignations", new[] { "Contact_Id" });
            CreateTable(
                "dbo.UserPermissions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PermissionId = c.Int(nullable: false),
                        AccessLevel = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        ApplicationUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Permissions", t => t.PermissionId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUser_Id)
                .Index(t => t.PermissionId)
                .Index(t => t.ApplicationUser_Id);
            
            DropTable("dbo.Attorneys");
            DropTable("dbo.Emails");
            DropTable("dbo.Phones");
            DropTable("dbo.Banks");
            DropTable("dbo.Contacts");
            DropTable("dbo.ManagerDesignations");
            DropTable("dbo.Courts");
            DropTable("dbo.MenuModels");
            DropTable("dbo.ProcessServicers");
            DropTable("dbo.Recorders");
            DropTable("dbo.Sheriffs");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Sheriffs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        BranchName = c.String(maxLength: 100),
                        Address_Street = c.String(maxLength: 200),
                        Address_Unit = c.String(maxLength: 100),
                        City = c.String(maxLength: 40),
                        County = c.String(maxLength: 100),
                        State = c.String(maxLength: 100),
                        StateCode = c.String(maxLength: 10),
                        ZIP = c.String(maxLength: 20),
                        ZIP4 = c.String(maxLength: 20),
                        Phone = c.String(maxLength: 40),
                        PhoneExt = c.String(maxLength: 20),
                        PhonePersonName = c.String(maxLength: 100),
                        Fax = c.String(maxLength: 40),
                        cell = c.String(maxLength: 40),
                        cellPersonName = c.String(maxLength: 100),
                        Email = c.String(maxLength: 40),
                        EmailPersonName = c.String(maxLength: 100),
                        WebSite = c.String(maxLength: 100),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Recorders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        BranchName = c.String(maxLength: 100),
                        Address_Street = c.String(maxLength: 200),
                        Address_Unit = c.String(maxLength: 100),
                        City = c.String(maxLength: 40),
                        District = c.String(maxLength: 100),
                        County = c.String(maxLength: 100),
                        State = c.String(maxLength: 100),
                        StateCode = c.String(maxLength: 10),
                        ZIP = c.String(maxLength: 20),
                        ZIP4 = c.String(maxLength: 10),
                        Phone = c.String(maxLength: 40),
                        PhoneExt = c.String(maxLength: 20),
                        PhonePersonName = c.String(maxLength: 100),
                        Fax = c.String(maxLength: 40),
                        cell = c.String(maxLength: 40),
                        cellPersonName = c.String(maxLength: 100),
                        Email = c.String(maxLength: 60),
                        EmailPersonName = c.String(maxLength: 100),
                        WebSite = c.String(maxLength: 100),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProcessServicers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        BranchName = c.String(maxLength: 100),
                        Address_Street = c.String(maxLength: 200),
                        Address_Unit = c.String(maxLength: 100),
                        City = c.String(nullable: false, maxLength: 100),
                        County = c.String(maxLength: 100),
                        State = c.String(maxLength: 100),
                        StateCode = c.String(maxLength: 10),
                        ZIP = c.String(maxLength: 20),
                        ZIP4 = c.String(maxLength: 20),
                        Phone = c.String(nullable: false, maxLength: 40),
                        PhoneExt = c.String(maxLength: 20),
                        PhonePersonName = c.String(maxLength: 100),
                        Fax = c.String(maxLength: 40),
                        cell = c.String(maxLength: 40),
                        CellPersonName = c.String(maxLength: 100),
                        Email = c.String(nullable: false, maxLength: 60),
                        EmailPersonName = c.String(maxLength: 100),
                        WebSite = c.String(maxLength: 100),
                        Facebook = c.String(maxLength: 100),
                        TwitterAccount = c.String(maxLength: 100),
                        LinkedIn = c.String(maxLength: 100),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MenuModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Action = c.String(),
                        Controller = c.String(),
                        ParentId = c.Int(nullable: false),
                        SortOrder = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Courts",
                c => new
                    {
                        CourtID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        BranchName = c.String(maxLength: 100),
                        Address_Street = c.String(maxLength: 200),
                        Address_Unit = c.String(maxLength: 100),
                        City = c.String(nullable: false, maxLength: 40),
                        County = c.String(maxLength: 100),
                        State = c.String(maxLength: 60),
                        StateCode = c.String(maxLength: 10),
                        District = c.String(maxLength: 100),
                        ZIP = c.String(maxLength: 20),
                        ZIP4 = c.String(maxLength: 10),
                        Phone = c.String(maxLength: 40),
                        PhoneExt = c.String(maxLength: 20),
                        Fax = c.String(maxLength: 40),
                        cell = c.String(maxLength: 40),
                        Email = c.String(maxLength: 60),
                        WebSite = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.CourtID);
            
            CreateTable(
                "dbo.ManagerDesignations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        Contact_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Contacts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        JobTitle = c.String(maxLength: 40),
                        JobTitleName = c.String(),
                        salutation = c.String(maxLength: 20),
                        firstname = c.String(nullable: false, maxLength: 100),
                        lastname = c.String(maxLength: 100),
                        Physical_Address_Street = c.String(maxLength: 100),
                        Physical_Address_Unit = c.String(maxLength: 100),
                        Physical_City = c.String(maxLength: 40),
                        Physical_County = c.String(maxLength: 40),
                        Physical_State = c.String(maxLength: 40),
                        Physical_StateCode = c.String(maxLength: 4),
                        Physical_ZIP = c.String(maxLength: 20),
                        Physical_ZIP4 = c.String(maxLength: 20),
                        Mailing_Address_Street = c.String(maxLength: 100),
                        Mailing_Address_Unit = c.String(maxLength: 100),
                        Mailing_City = c.String(maxLength: 40),
                        Mailing_County = c.String(maxLength: 40),
                        Mailing_State = c.String(maxLength: 40),
                        Mailing_StateCode = c.String(maxLength: 4),
                        Mailing_ZIP = c.String(maxLength: 20),
                        Mailing_ZIP4 = c.String(maxLength: 20),
                        Phone = c.String(maxLength: 60),
                        PhoneExt = c.String(maxLength: 20),
                        Fax = c.String(maxLength: 40),
                        cell = c.String(maxLength: 100),
                        Email = c.String(maxLength: 20),
                        IsVerify = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Banks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        BranchName = c.String(maxLength: 100),
                        BankDepartment = c.String(maxLength: 100),
                        Address_Street = c.String(maxLength: 200),
                        Address_Unit = c.String(maxLength: 100),
                        County = c.String(maxLength: 100),
                        City = c.String(nullable: false, maxLength: 100),
                        State = c.String(maxLength: 100),
                        StateCode = c.String(maxLength: 4),
                        ZIP = c.String(maxLength: 20),
                        ZIP4 = c.String(maxLength: 10),
                        Phone = c.String(nullable: false, maxLength: 40),
                        PhoneExt = c.String(maxLength: 20),
                        Phone1 = c.String(maxLength: 40),
                        Phone2 = c.String(maxLength: 40),
                        Phone3 = c.String(maxLength: 40),
                        Fax = c.String(maxLength: 40),
                        cell = c.String(maxLength: 40),
                        Email = c.String(maxLength: 60),
                        Email1 = c.String(maxLength: 60),
                        Email2 = c.String(maxLength: 60),
                        Email3 = c.String(maxLength: 60),
                        WebSite = c.String(maxLength: 100),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Phones",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ParentID = c.String(),
                        PhoneNumber = c.String(nullable: false, maxLength: 40),
                        PhoneExt = c.String(maxLength: 10),
                        PhonePersonName = c.String(maxLength: 100),
                        ParentModule = c.String(),
                        IsVerify = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        Attorney_Id = c.Int(),
                        Bank_Id = c.Int(),
                        Contact_Id = c.Int(),
                        Court_CourtID = c.Int(),
                        ProcessServicer_Id = c.Int(),
                        Recorder_Id = c.Int(),
                        Sheriff_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Emails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ParentID = c.String(),
                        EmailAddress = c.String(nullable: false, maxLength: 40),
                        PersonName = c.String(maxLength: 100),
                        ParentModule = c.String(),
                        IsPrimary = c.Boolean(nullable: false),
                        IsVerify = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        Attorney_Id = c.Int(),
                        Bank_Id = c.Int(),
                        Contact_Id = c.Int(),
                        Court_CourtID = c.Int(),
                        ProcessServicer_Id = c.Int(),
                        Recorder_Id = c.Int(),
                        Sheriff_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Attorneys",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Bar = c.String(nullable: false, maxLength: 10),
                        SBN = c.String(nullable: false, maxLength: 12),
                        Phone = c.String(nullable: false, maxLength: 40),
                        PhoneExt = c.String(maxLength: 20),
                        Fax = c.String(maxLength: 40),
                        cell = c.String(maxLength: 40),
                        Email = c.String(nullable: false, maxLength: 60),
                        Facebook = c.String(maxLength: 100),
                        TwitterAccount = c.String(maxLength: 100),
                        LinkedIn = c.String(maxLength: 100),
                        Rate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            DropForeignKey("dbo.UserPermissions", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserPermissions", "PermissionId", "dbo.Permissions");
            DropIndex("dbo.UserPermissions", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.UserPermissions", new[] { "PermissionId" });
            DropTable("dbo.UserPermissions");
            CreateIndex("dbo.ManagerDesignations", "Contact_Id");
            CreateIndex("dbo.Phones", "Sheriff_Id");
            CreateIndex("dbo.Phones", "Recorder_Id");
            CreateIndex("dbo.Phones", "ProcessServicer_Id");
            CreateIndex("dbo.Phones", "Court_CourtID");
            CreateIndex("dbo.Phones", "Contact_Id");
            CreateIndex("dbo.Phones", "Bank_Id");
            CreateIndex("dbo.Phones", "Attorney_Id");
            CreateIndex("dbo.Emails", "Sheriff_Id");
            CreateIndex("dbo.Emails", "Recorder_Id");
            CreateIndex("dbo.Emails", "ProcessServicer_Id");
            CreateIndex("dbo.Emails", "Court_CourtID");
            CreateIndex("dbo.Emails", "Contact_Id");
            CreateIndex("dbo.Emails", "Bank_Id");
            CreateIndex("dbo.Emails", "Attorney_Id");
            AddForeignKey("dbo.Phones", "Sheriff_Id", "dbo.Sheriffs", "Id");
            AddForeignKey("dbo.Emails", "Sheriff_Id", "dbo.Sheriffs", "Id");
            AddForeignKey("dbo.Phones", "Recorder_Id", "dbo.Recorders", "Id");
            AddForeignKey("dbo.Emails", "Recorder_Id", "dbo.Recorders", "Id");
            AddForeignKey("dbo.Phones", "ProcessServicer_Id", "dbo.ProcessServicers", "Id");
            AddForeignKey("dbo.Emails", "ProcessServicer_Id", "dbo.ProcessServicers", "Id");
            AddForeignKey("dbo.Phones", "Court_CourtID", "dbo.Courts", "CourtID");
            AddForeignKey("dbo.Emails", "Court_CourtID", "dbo.Courts", "CourtID");
            AddForeignKey("dbo.Phones", "Contact_Id", "dbo.Contacts", "Id");
            AddForeignKey("dbo.ManagerDesignations", "Contact_Id", "dbo.Contacts", "Id");
            AddForeignKey("dbo.Emails", "Contact_Id", "dbo.Contacts", "Id");
            AddForeignKey("dbo.Phones", "Bank_Id", "dbo.Banks", "Id");
            AddForeignKey("dbo.Emails", "Bank_Id", "dbo.Banks", "Id");
            AddForeignKey("dbo.Phones", "Attorney_Id", "dbo.Attorneys", "Id");
            AddForeignKey("dbo.Emails", "Attorney_Id", "dbo.Attorneys", "Id");
        }
    }
}
