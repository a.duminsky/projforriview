namespace Dextera.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRoleListToUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.GroupRoles", "ApplicationUser_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.GroupRoles", "ApplicationUser_Id");
            AddForeignKey("dbo.GroupRoles", "ApplicationUser_Id", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.GroupRoles", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.GroupRoles", new[] { "ApplicationUser_Id" });
            DropColumn("dbo.GroupRoles", "ApplicationUser_Id");
        }
    }
}
