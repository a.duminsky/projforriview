namespace Dextera.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tenants1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tenants", "ClientType", c => c.Int(nullable: false));
            AddColumn("dbo.Tenants", "SubClientType", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tenants", "SubClientType");
            DropColumn("dbo.Tenants", "ClientType");
        }
    }
}
