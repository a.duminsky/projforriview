namespace Dextera.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixNew : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.GroupRoles", "GroupId", "dbo.Groups");
            DropIndex("dbo.GroupRoles", new[] { "GroupId" });
            DropTable("dbo.GroupRoles");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.GroupRoles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        GroupId = c.Int(nullable: false),
                        RoleId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.GroupRoles", "GroupId");
            AddForeignKey("dbo.GroupRoles", "GroupId", "dbo.Groups", "Id", cascadeDelete: true);
        }
    }
}
