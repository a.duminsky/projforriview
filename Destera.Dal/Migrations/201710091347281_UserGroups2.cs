namespace Dextera.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserGroups2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.GroupUsers", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.GroupUsers", new[] { "ApplicationUser_Id" });
            DropColumn("dbo.GroupUsers", "UserId");
            RenameColumn(table: "dbo.GroupUsers", name: "ApplicationUser_Id", newName: "UserId");
            AlterColumn("dbo.GroupUsers", "UserId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.GroupUsers", "UserId", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.GroupUsers", "UserId");
            AddForeignKey("dbo.GroupUsers", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.GroupUsers", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.GroupUsers", new[] { "UserId" });
            AlterColumn("dbo.GroupUsers", "UserId", c => c.String(maxLength: 128));
            AlterColumn("dbo.GroupUsers", "UserId", c => c.String(nullable: false));
            RenameColumn(table: "dbo.GroupUsers", name: "UserId", newName: "ApplicationUser_Id");
            AddColumn("dbo.GroupUsers", "UserId", c => c.String(nullable: false));
            CreateIndex("dbo.GroupUsers", "ApplicationUser_Id");
            AddForeignKey("dbo.GroupUsers", "ApplicationUser_Id", "dbo.AspNetUsers", "Id");
        }
    }
}
