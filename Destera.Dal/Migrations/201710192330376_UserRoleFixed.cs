namespace Dextera.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserRoleFixed : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.RoleUsers", "User_Id", "dbo.AspNetUsers");
            DropIndex("dbo.RoleUsers", new[] { "User_Id" });
            DropColumn("dbo.RoleUsers", "UserId");
            RenameColumn(table: "dbo.RoleUsers", name: "User_Id", newName: "UserId");
            AlterColumn("dbo.RoleUsers", "UserId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.RoleUsers", "UserId", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.RoleUsers", "UserId");
            AddForeignKey("dbo.RoleUsers", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RoleUsers", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.RoleUsers", new[] { "UserId" });
            AlterColumn("dbo.RoleUsers", "UserId", c => c.String(maxLength: 128));
            AlterColumn("dbo.RoleUsers", "UserId", c => c.Int(nullable: false));
            RenameColumn(table: "dbo.RoleUsers", name: "UserId", newName: "User_Id");
            AddColumn("dbo.RoleUsers", "UserId", c => c.Int(nullable: false));
            CreateIndex("dbo.RoleUsers", "User_Id");
            AddForeignKey("dbo.RoleUsers", "User_Id", "dbo.AspNetUsers", "Id");
        }
    }
}
