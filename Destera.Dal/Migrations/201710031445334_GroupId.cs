namespace Dextera.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GroupId : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.GroupUsers", "UserId", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.GroupUsers", "UserId", c => c.Int(nullable: false));
        }
    }
}
