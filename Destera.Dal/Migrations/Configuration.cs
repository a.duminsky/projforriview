﻿using Dextera.Dal.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dextera.Dal.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<DexteraDbContext>
    {
        private int TenantId { get; set; }
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            
        }

        protected override void Seed(DexteraDbContext context)
        {

            FillUsers(context);
            FillTenants(context);
            FillPermissions(context);
            SeedStaticData(context);
            FillAssessments(context);
            base.Seed(context);
        }

        private void SeedStaticData(DexteraDbContext context)
        {
            //context.Users.Add(new ApplicationUser());
            context.SaveChanges();
        }

        private void FillUsers(DexteraDbContext context)
        {
            var userStore = new UserStore<ApplicationUser>(context);
            var manager = new UserManager<ApplicationUser>(userStore);
            userStore.AutoSaveChanges = true;
            CreateUserIfNotExist(new ApplicationUser() { Email = "test@volya.com", UserName = "test@volya.com" }, manager);

        }

        private void CreateUserIfNotExist(ApplicationUser user, UserManager<ApplicationUser> manager)
        {

            var dbUser = manager.FindByEmail(user.Email);
            if (dbUser == null)
            {
                var res = manager.Create(user, "password");
            }
        }

        private void FillPermissions(DexteraDbContext context)
        {
            DeletePermission("UsersMagement", context);
            DeletePermission("RolesManagement", context);
            DeletePermission("Chart", context);
            var permissions = new string[] {"Create new case", "Modify Case Data", "Delete Case Data", "Encrypted Fields", "Run Reports / Create",
                "Print", "Create Alerts", "Create Invoices", "Modify Workflow", "Create Calendar Items", "Email Clients", "Create Templates",
                "Review Billing", "Set Goals", "Review Goals", "Banking Info", "PII", "Assign Tasks" };

            var permissionsWithoutAccessLevel = new string[] {"Create new case", "Print", "Create Alerts", "Create Calendar Items", "Email Client",
            "Set Goals", "Review Goals" };
            foreach (var p in permissions)
            {
                CreatePermission(new Permission() { Title = p, AccessLevelRequire = !permissionsWithoutAccessLevel.Any(x => x == p) }, context);
            }
        }

        private void DeletePermission(string name, DexteraDbContext context)
        {
            var obj = context.Permissions.FirstOrDefault(x => x.Title == name);
            if (obj != null)
            {
                context.Permissions.Remove(obj);
                context.SaveChanges();
            }
        }

        private void CreatePermission(Permission p, DexteraDbContext context)
        {
            var obj = context.Permissions.FirstOrDefault(x => x.Title == p.Title);
            if (obj == null)
            {
                p.CreatedAt = DateTime.Now;
                p.UpdatedAt = DateTime.Now;

                var res = context.Permissions.Add(p);
                context.SaveChanges();
            }
        }


        private void FillTenants(DexteraDbContext context)
        {
            TenantId = CreateTenant(new Tenant() { ClientName = "test" }, context);
        }

        

        private int CreateTenant(Tenant p, DexteraDbContext context)
        {
            p.CreatedAt = DateTime.Now;
            p.UpdatedAt = DateTime.Now;

            var obj = context.Tenants.FirstOrDefault(x => x.ClientName == p.ClientName);
            if (obj == null)
            {
                var res = context.Tenants.Add(p);
                context.SaveChanges();
                return res.Id;
            }
            else
            {
                return obj.Id;
            }




        }

        private void FillAssessments(DexteraDbContext context)
        {
            //[  ]Monthly  [  ]Quarterly  [  ]Semi‐Annual  [  ]Annual 
            CreateAssessment(new Assessment() { Name = "Monthly" }, context);
            CreateAssessment(new Assessment() { Name = "Quarterly" }, context);
            CreateAssessment(new Assessment() { Name = "Semi‐Annua" }, context);
            CreateAssessment(new Assessment() { Name = "Annual" }, context);
            
        }

      




        private int CreateAssessment(Assessment p, DexteraDbContext context)
        {
            p.CreatedAt = DateTime.Now;
            p.UpdatedAt = DateTime.Now;

            var obj = context.Assessments.FirstOrDefault(x => x.Name == p.Name);
            if (obj == null)
            {
                var res = context.Assessments.Add(p);
                context.SaveChanges();
                return res.Id;
            }
            else
            {
                return obj.Id;
            }
        }

        private void FillAttorney(DexteraDbContext context)
        {
            //[  ]Monthly  [  ]Quarterly  [  ]Semi‐Annual  [  ]Annual 
            CreateAssessment(new Attorney() { Name = "Test1Attorney" }, context);
            CreateAssessment(new Attorney() { Name = "Test2Attorney" }, context);

        }
        private int CreateAssessment(Attorney p, DexteraDbContext context)
        {
            p.CreatedAt = DateTime.Now;
            p.UpdatedAt = DateTime.Now;

            var obj = context.Assessments.FirstOrDefault(x => x.Name == p.Name);
            if (obj == null)
            {
                var res = context.Attorney.Add(p);
                context.SaveChanges();
                return res.Id;
            }
            else
            {
                return obj.Id;
            }
        }

    }
}
