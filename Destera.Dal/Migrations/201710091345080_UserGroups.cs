namespace Dextera.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserGroups : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.GroupUsers", "ApplicationUser_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.GroupUsers", "ApplicationUser_Id");
            AddForeignKey("dbo.GroupUsers", "ApplicationUser_Id", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.GroupUsers", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.GroupUsers", new[] { "ApplicationUser_Id" });
            DropColumn("dbo.GroupUsers", "ApplicationUser_Id");
        }
    }
}
