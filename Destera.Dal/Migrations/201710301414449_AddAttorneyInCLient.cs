namespace Dextera.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAttorneyInCLient : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Attorneys",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Clients", "AttorneyId", c => c.Int());
            CreateIndex("dbo.Clients", "AttorneyId");
            AddForeignKey("dbo.Clients", "AttorneyId", "dbo.Attorneys", "Id");
            DropColumn("dbo.Clients", "Attorney");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Clients", "Attorney", c => c.Int());
            DropForeignKey("dbo.Clients", "AttorneyId", "dbo.Attorneys");
            DropIndex("dbo.Clients", new[] { "AttorneyId" });
            DropColumn("dbo.Clients", "AttorneyId");
            DropTable("dbo.Attorneys");
        }
    }
}
