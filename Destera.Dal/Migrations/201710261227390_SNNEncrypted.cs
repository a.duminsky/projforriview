namespace Dextera.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SNNEncrypted : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "SNNEncrypted", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "SNNEncrypted");
        }
    }
}
