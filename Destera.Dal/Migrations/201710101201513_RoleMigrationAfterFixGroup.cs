namespace Dextera.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RoleMigrationAfterFixGroup : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GroupRoles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        GroupId = c.Int(nullable: false),
                        RoleId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Groups", t => t.GroupId, cascadeDelete: true)
                .ForeignKey("dbo.Roles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.GroupId)
                .Index(t => t.RoleId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.GroupRoles", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.GroupRoles", "GroupId", "dbo.Groups");
            DropIndex("dbo.GroupRoles", new[] { "RoleId" });
            DropIndex("dbo.GroupRoles", new[] { "GroupId" });
            DropTable("dbo.GroupRoles");
        }
    }
}
