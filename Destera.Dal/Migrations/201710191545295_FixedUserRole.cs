namespace Dextera.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixedUserRole : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.GroupRoles", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.GroupRoles", new[] { "ApplicationUser_Id" });
            CreateTable(
                "dbo.RoleUsers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RoleId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Roles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.RoleId)
                .Index(t => t.User_Id);
            
            DropColumn("dbo.GroupRoles", "ApplicationUser_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.GroupRoles", "ApplicationUser_Id", c => c.String(maxLength: 128));
            DropForeignKey("dbo.RoleUsers", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.RoleUsers", "RoleId", "dbo.Roles");
            DropIndex("dbo.RoleUsers", new[] { "User_Id" });
            DropIndex("dbo.RoleUsers", new[] { "RoleId" });
            DropTable("dbo.RoleUsers");
            CreateIndex("dbo.GroupRoles", "ApplicationUser_Id");
            AddForeignKey("dbo.GroupRoles", "ApplicationUser_Id", "dbo.AspNetUsers", "Id");
        }
    }
}
