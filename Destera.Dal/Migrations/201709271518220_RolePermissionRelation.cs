namespace Dextera.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RolePermissionRelation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Permissions", "RoleId", c => c.Int(nullable: false));
            CreateIndex("dbo.Permissions", "RoleId");
            AddForeignKey("dbo.Permissions", "RoleId", "dbo.Roles", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Permissions", "RoleId", "dbo.Roles");
            DropIndex("dbo.Permissions", new[] { "RoleId" });
            DropColumn("dbo.Permissions", "RoleId");
        }
    }
}
