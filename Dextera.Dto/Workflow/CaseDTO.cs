﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dextera.Dto.Workflow
{
    public class CaseDTO
    {
        public int Id { get; set; }
        public string AddressStreet { get; set; }
        public string AddressUnit { get; set; }

        public string AddressCity { get; set; }
        public string AddressState { get; set; }
        public string AddressZip { get; set; }
        public string AddressPhone { get; set; }

        public int? ClientId { get; set; }

        public bool AdvanceRetainerReceived { get; set; }
    }
}
