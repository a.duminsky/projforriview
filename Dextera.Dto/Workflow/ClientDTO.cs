﻿using Dextera.Dal.Models;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System;
using Dextera.Common.Enums;


namespace Dextera.Dto.Workflow
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ClientDTO 
    {
        //Client 
        public int Id { get; set; }
        [Required]
        public string ClientName { get; set; }
        //public ClientType ClientType { get; set; }
        public string ClientAddress { get; set; }
        public string CityStateZip { get; set; }
        public string GeneralPhone { get; set; }
        public string Fax { get; set; }
        public string Website { get; set; }
        public string LegalName { get; set; }
        //public int? Attorney { get; set; }
        public bool Annual { get; set; }
        public DateTime? Signed { get; set; }
        [Required]
        public int? AttorneyId { get; set; }
        
        public int? SubClientId { get; set; }
        public virtual Client SubClient { get; set; } //add subclient table




        //[Required]
        //public string SubClientName { get; set; }
        //public ClientType SubClientType { get; set; }
        //Contact

        // name, role? откуда брать? И все поля должны с юзер совпадать?
        public string Address { get; set; }
        public string CityStateZip4Second { get; set; }
        public string PhoneExt { get; set; }
        public string Mobile { get; set; }
        public string EmailContact { get; set; }
        //public List<int> Role { get; set; }


        //Financial
        public string CostAdvanceAmountMoney { get; set; }
        public bool CostAdvanceRB { get; set; }
        public string RetainerAmountMoney { get; set; }
        public bool RetainerRB { get; set; }
        public bool AssessmentFeesRB { get; set; }
        public string AssessmentMoney { get; set; }

        [Required]
        public int AssessmentId { get; set; }
        
        public string HOAInterestPercent { get; set; } // ?? такой тип
        public string LateFeeMoney { get; set; } // ?? ++ add table
        public string LateFeePercent { get; set; }
        public string AssessmentIncMoney { get; set; }
        public string AssessmentIncPercent { get; set; }
        public bool InterestIncreaseRB { get; set; }
        public string InterestIncreasePercent { get; set; }
        public bool LateFeeIncreaseRB { get; set; }
        public string LateFeeIncreaseMoney { get; set; }
        public DateTime? IncreaseStartDate { get; set; }
        public string SpecialAssessmentMoney { get; set; }
        public DateTime? SpecialAssessmentStartDate { get; set; }
        public DateTime? SpecialAssessmentEndDate { get; set; }
        //public int SpecialAssessmentId { get; set; }
        
        public bool SpecialBillingRB { get; set; }
        public string AttorneyRateMoney { get; set; }
        public int? AttorneyRateId { get; set; }
        public string CourtFeesMoney { get; set; }
        //public int CourtFeesId { get; set; }
        public string CostAdvanceFeesMoney { get; set; }
        //public int CostAdvanceFeesId { get; set; }
        public virtual CostAdvanceFees CostAdvanceFees { get; set; } // правильно тут?, 

        //Settlement Provisions
        //public int LengthPaymentId { get; set; }
        public virtual LengthPayment LengthPayment { get; set; }  // правильно тут?, 
        public string DownPaymentMoney { get; set; }
        public string DownPaymentPercent { get; set; }
        //public int FeesThatId { get; set; }
        public virtual FeesThat FeesThat { get; set; }  // правильно тут?, 

        public string WillSettlePercent { get; set; }
        //public int WillSettleId { get; set; }
        public virtual WillSettle WillSettle { get; set; }  // правильно тут?, 

    }
}