﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dextera.Dto.Common
{
    public class SelectOptionDTO<K>
    {
        public K Id { get; set; }
        public string Name { get; set; }
    }
}
