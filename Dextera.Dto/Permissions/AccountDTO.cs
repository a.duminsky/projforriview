﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dextera.Dto.Permissions
{
    public class AccountDTO
    {
        public string Id { get; set; }
        

        public string Password { get; set; }
        public string Phone { get; set; }
        public string Hometown { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string salt { get; set; }
        public string Alias { get; set; }
        public string Bar { get; set; }
        public string IM { get; set; }

        public List<int> Groups { get; set; }
        public List<int> Roles { get; set; }
        //public string PhonePersonName { get; set; }

        public string PhoneExt { get; set; }

        public string Phones
        {
            get
            {
                if (PhoneExt != null)
                {
                    return Phone + " - " + PhoneExt;
                }
                else
                {
                    return Phone;
                }
            }
        }



        [Required]
        public string Email { get; set; }

       

        public int? TenantId { get; set; }

        public string SNN { get; set; }
    }
}
