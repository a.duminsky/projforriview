﻿using Dextera.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dextera.Dto.Permissions
{
    public class PermissionsDTO
    {

        public int Id { get; set; }

        public string Title { get; set; }

        public PermissionType Type { get; set; }
        public bool AccessLevelRequire { get; set; }

        public bool Checked { get; set; }
        public AccessLevel AccessLevel { get; set; }
    }
}
