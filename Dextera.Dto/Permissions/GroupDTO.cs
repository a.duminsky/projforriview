﻿using Dextera.Common.Enums;
using Dextera.Dal.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dextera.Dto.Permissions
{
    public class GroupDTO
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
        public List<int> Roles { get; set; }
    }
}
