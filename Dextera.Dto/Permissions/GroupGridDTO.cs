﻿using Dextera.Common.Enums;
using Dextera.Dal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dextera.Dto.Permissions
{
    public class GroupGridDTO
    {
        public List<GroupDTO> Groups { get; set; }
    }
}
