﻿using Dextera.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dextera.Dto.Permissions
{
    public class RolePermissionsDTO : RoleDTO
    {
        public List<PermissionsDTO> Permissions { get; set; }
    }
}
