﻿using Dextera.Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dextera.Dto.Permissions
{
    public class TenantDTO
    {

        public int Id { get; set; }
        [Required]
        public string ClientName { get; set; }

        public ClientType ClientType { get; set; }
        public string ClientAddress { get; set; }

        public string ClientCity { get; set; }
        public string ClientState { get; set; }

        public string ClientZip { get; set; }

        public string ClientCountry { get; set; }

        public string ClientPhone { get; set; }

        public string ClientFax { get; set; }

        public string ClientWebSite { get; set; }

        [Required]
        public string SubClientName { get; set; }

        public ClientType SubClientType { get; set; }
        public string SubClientAddress { get; set; }

        public string SubClientCity { get; set; }
        public string SubClientState { get; set; }

        public string SubClientZip { get; set; }

        public string SubClientCountry { get; set; }

        public string SubClientPhone { get; set; }

        public string SubClientFax { get; set; }

        public string SubClientWebSite { get; set; }

    }
}
