﻿using Dextera.Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dextera.Dto.Permissions
{
    public class RoleDTO
    {

        public int Id { get; set; }

        [StringLength(60, MinimumLength = 4)]
        [Required]
        public string Name { get; set; }
    }
}
