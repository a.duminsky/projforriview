﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Dextera.Web.Controllers
{
    
    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            return this.Redirect("index.html");
        }
    }
}
