﻿/* Firm Setup page controller */
angular.module('DexteraApp').controller('FirmListController', ['$rootScope', '$scope', '$state', 'settings', 'firmService', function ($rootScope, $scope, $state, settings, firmService) {
    $scope.$on('$viewContentLoaded', function() {   
        // initialize core components
        App.initAjax();

        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;
    });

    function generateButtonsColumn(dataItem) {
        return '<a href="" class="k-button" data-ng-click="edit('+dataItem.id+')">Edit</a>';
    };

    $scope.add = function () {
        $state.go('firm');
    }

    $scope.edit = function (id) {
        $state.go('firm', { id: id });
    }

    $scope.options = {
        height: 650,
        columns: [{ field: "ClientName", title: "Client Name", width: "100px" },
            { field: "ClientAddress", title: "Client Address", width: "100px" },
            { field: "ClientCity", title: "Client City", width: "100px" },
            { field: "ClientState", title: "Client State", width: "100px" },
            { field: "ClientZip", title: "Client Zip", width: "100px" },
            { field: "ClientCountry", title: "Client Country", width: "100px" },
            { field: "ClientState", title: "Client State", width: "100px" },
            { field: "SubClientName", title: "Sub Client Name", width: "100px" },
            { field: "SubClientAddress", title: "Sub Client Address", width: "100px" },
            { field: "SubClientCity", title: "Sub Client City", width: "100px" },
            { field: "SubClientState", title: "Sub Client State", width: "100px" },
            { field: "SubClientZip", title: "Sub Client Zip", width: "100px" },
            { field: "SubClientCountry", title: "Sub Client Country", width: "100px" },
            { template: generateButtonsColumn, width: 100 }],
        dataSource: {
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
            pageSize: 10,
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "Id",
                    fields: {
                        //Id: {type:"number"},
                        ClientName: { editable: false, type: "string" },
                        ClientAddress: { editable: false, type: "string" },
                        ClientCity: { editable: false, type: "string" },
                        ClientState: { editable: false, type: "string" },
                        ClientZip: { editable: false, type: "string" },
                        ClientCountry: { editable: false, type: "string" },
                        SubClientName: { editable: false, type: "string" },
                        SubClientAddress: { editable: false, type: "string" },
                        SubClientCity: { editable: false, type: "string" },
                        SubClientState: { editable: false, type: "string" },
                        SubClientZip: { editable: false, type: "string" },
                        SubClientCountry: { editable: false, type: "string" },
                    }
                }
            },
            error: function (e) {
                var msg = e.xhr.responseText;
                $scope.savedSuccessfully = false;
                $scope.message = msg;
            },
            transport: {
                read: function (options) {
                    firmService.getList(options.data).then(function (result) {
                        options.success(result.data);
                    })
                    
                },
                parameterMap: function (data, operation) {
                    return JSON.stringify(data);
                },
            }
        },
        
        pageable: true,
        sortable: true,
        filterable: true,
        
    };

}]);
