/* Setup blank page controller */
angular.module('DexteraApp').controller('GroupController', ['$rootScope', '$scope', '$state', '$stateParams', 'settings', 'roleService', 'groupService', 'notificationService',
    function ($rootScope, $scope, $state, $stateParams, settings, roleService, groupService, notificationService) {
        $scope.$on('$viewContentLoaded', function () {
            // initialize core components
            App.initAjax();

            // set default layout mode
            $rootScope.settings.layout.pageContentWhite = true;
            $rootScope.settings.layout.pageBodySolid = false;
            $rootScope.settings.layout.pageSidebarClosed = false;
        });

        $scope.get = function () {
            if ($stateParams.id) {
                groupService.get($stateParams.id).then(function (result) {
                    $scope.dataGroup = result.data;
                })
            }
        }
        $scope.get();


        $scope.cancel = function () {
            $state.go("manageGroups")
        }


        $scope.groupServiceUpdate = function () {
            if ($stateParams.id) {
                groupService.update($scope.dataGroup).then(function () {
                    notificationService.showSuccess("Group saved sucessfully!");
                    $state.go("manageGroups");
                }, $scope.showValidation);
            }
            else {
                groupService.add($scope.dataGroup).then(function () {
                    notificationService.showSuccess("Group saved sucessfully!");
                    $state.go("manageGroups");
                });
            }
        };

        $scope.rolesOptions = {
            placeholder: "Select roles...",
            dataTextField: "Name",
            dataValueField: "Id",
            valuePrimitive: true,
            autoBind: false,
            dataSource: {
                type: "odata",
                serverFiltering: true,
                transport: {
                    read: function (options) {
                        roleService.getList(options.data).then(function (result) {
                            options.success({
                                "d": {
                                    "results": result.data.Data, "__count": result.data.Count
                                }
                            })
                        })

                    },
                    parameterMap: function (data, operation) {
                        return JSON.stringify(data);
                    },
                }
            }
        };
        $scope.get(); 

    }]);
