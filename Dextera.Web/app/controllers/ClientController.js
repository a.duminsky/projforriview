﻿/* Firm Setup page controller */
angular.module('DexteraApp').controller('ClientController', ['$rootScope', '$scope', '$state', '$stateParams', 'settings', 'clientService', 'selectService', 'notificationService',
    function ($rootScope, $scope, $state, $stateParams, settings, clientService, selectService, notificationService) {
    $scope.$on('$viewContentLoaded', function() {   
        // initialize core components
        App.initAjax();

        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;
       
    });
    $scope.dataClient = {}; //AssessmentId: 1, AttorneyId:1
    $scope.get = function () {
        if ($stateParams.id) {
            clientService.get($stateParams.id).then(function (result) {
                $scope.dataClient = result.data;
            })
        }
    }
    $scope.getSubClient = function () {
        if ($stateParams.id) {
            clientService.get($stateParams.id).then(function (result) {
                $scope.dataSubClients = result.data;
            })
        }
    }



    $scope.save = function () {
        if ($stateParams.id)
        {
            clientService.update($scope.dataClient).then(function () {
                notificationService.showSuccess("Client saved sucessfully!");
                $state.go("manageClients")
            }, $scope.showValidation);
        }
        else {
            clientService.add($scope.dataClient).then(function () {
                notificationService.showSuccess("Client saved sucessfully!");
                $state.go("manageClients")
            }, $scope.showValidation);
        }
    }

    
    
    selectService.getSelectOptions('assessment', 'assessment').then(function (res) {
        $scope.AssessmentOptions = res;
        })
    selectService.getSelectOptions('attorney', 'attorney').then(function (res) {
        $scope.AttorneyOptions = res;
    })

    selectService.getSelectOptions('assessment', 'assessment').then(function (res) {
        $scope.SpecialAssessmentOptions = res;
    })




    $scope.SubClientOptions = selectService.getSelectOptions('client', 'client')
     
    $scope.get();
  
    //$scope.get();

}]);
