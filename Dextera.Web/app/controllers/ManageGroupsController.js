﻿/* Firm Setup page controller */
angular.module('DexteraApp').controller('ManageGroupsController', ['$rootScope', '$scope', '$stateParams', 'settings',  'groupService','$state',
    'notificationService',
    function ($rootScope, $scope,$stateParams, settings,  groupService, $state, notificationService) {
        $scope.$on('$viewContentLoaded', function () {
            // initialize core components
            App.initAjax();

            // set default layout mode
            $rootScope.settings.layout.pageContentWhite = true;
            $rootScope.settings.layout.pageBodySolid = false;
            $rootScope.settings.layout.pageSidebarClosed = false;
        });

        function generateButtonsColumn(dataItem) {
            return '<a href="" class="k-button" data-ng-click="edit(\'' + dataItem.id + '\')">Edit</a>';
        };

        $scope.add = function () {
            $state.go('group');
        }

        $scope.edit = function (id) {
            $state.go('group', { id: id });
        }


        $scope.optionsGroup = {
            width: 150,
            height: 650,
            columns: [{ field: "Name", title: "Name", width: "500px" },
            { template: generateButtonsColumn, width: 100 }],
            dataSource: {
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                pageSize: 10,
                schema: {
                    data: "Data",
                    total: "Total",
                    model: {
                        id: "Id",
                        fields: {
                            Name: { editable: false, type: "string" },
                        }
                    }
                },
                error: function (e) {
                    var msg = e.xhr.responseText;
                    $scope.savedSuccessfully = false;
                    $scope.message = msg;
                },
                transport: {
                    read: function (options) {
                        groupService.getList(options.data).then(function (data) {
                            options.success(data.data);
                        })

                    },
                    parameterMap: function (data, operation) {
                        return JSON.stringify(data);
                    },
                }
            },

            pageable: true,
            sortable: true,
            filterable: true,
            //editable: "inline",
        };
    }]);
