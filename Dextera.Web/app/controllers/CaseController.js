﻿/* Firm Setup page controller */
angular.module('DexteraApp').controller('CaseController', ['$rootScope', '$scope', '$state', '$stateParams', 'settings', 'caseService', 'selectService', 'defenantService', 'notificationService',
    function ($rootScope, $scope, $state, $stateParams, settings, caseService, selectService, defenantService, notificationService) {
    $scope.$on('$viewContentLoaded', function() {   
        // initialize core components
        App.initAjax();

        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;

    });
    
    $scope.get = function () {
        $scope.case = {};
        if ($stateParams.id) {
            caseService.get($stateParams.id).then(function (result) {
                $scope.case = result.data;
            })
        }
    }

    function generateButtonsColumn(dataItem) {
        return '<a href="" class="k-button" data-ng-click="edit(' + dataItem.id + ')">Edit</a>';
    };
    
    $scope.save = function (state) {
        if ($stateParams.id)
        {
            caseService.update($scope.case).then(function () {
                notificationService.showSuccess("Client saved sucessfully!");
                $state.go("manageCases")
            }, $scope.showValidation);
        }
        else {
            caseService.add($scope.case).then(function () {
                notificationService.showSuccess("Client saved sucessfully!");
                $state.go("manageCases")
            }, $scope.showValidation);
        }
    }

    $scope.options = {
        height: 650,
        columns: [
            { field: "FirstName", title: "First Name", width: "100px" },
            { field: "MiddleName", title: "Middle Name", width: "100px" },
            { field: "LastName", title: "Last Name", width: "100px" },
            { field: "AddressCity", title: "Address City", width: "100px" },
            { field: "AddressState", title: "Address State", width: "100px" },
            { field: "AddressStreet", title: "Address Street", width: "100px" },
            { field: "AddressUnit", title: "Address Unit", width: "100px" },
            { field: "AddressCity", title: "Address City", width: "100px" },
            { field: "AddressState", title: "Address State", width: "100px" },
            { field: "AddressZip", title: "Address Zip", width: "100px" },
            { field: "AddressPhone", title: "Address Country", width: "100px" },

        { template: generateButtonsColumn, width: 100 }],
        dataSource: {
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
            pageSize: 10,
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "Id",
                    fields: {
                        Id: { type: "number" },
                        FirstName: { editable: false, type: "string" },
                        MiddleName: { editable: false, type: "string" },
                        LastName: { editable: false, type: "string" },

                        AddressStreet: { editable: false, type: "string" },
                        AddressUnit: { editable: false, type: "string" },
                        AddressCity: { editable: false, type: "string" },
                        AddressState: { editable: false, type: "string" },
                        AddressZip: { editable: false, type: "string" },
                        AddressPhone: { editable: false, type: "string" },

                    }
                }
            },
            error: function (e) {
                var msg = e.xhr.responseText;
                $scope.savedSuccessfully = false;
                $scope.message = msg;
            },
            transport: {
                read: function (options) {
                    if ($stateParams.id) {
                        defenantService.getList(options.data, $stateParams.id).then(function (result) {
                            options.success(result.data);
                        })
                    }
                    else {
                        options.success({});
                    }

                },
                parameterMap: function (data, operation) {
                    return JSON.stringify(data);
                },
            }
        },

        pageable: true,
        sortable: true,
        filterable: true,

    };
    selectService.getSelectOptions('client', 'client').then(function (res) {
        $scope.ClientOptions = res.data;
    })
    
    $scope.get();
    

}]);
