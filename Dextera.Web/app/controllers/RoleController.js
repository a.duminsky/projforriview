﻿/* Firm Setup page controller */
angular.module('DexteraApp').controller('RoleController', ['$rootScope', '$scope', '$state', 'settings', 'roleService', function ($rootScope, $scope, $state, settings, roleService) {
    $scope.$on('$viewContentLoaded', function() {   
        // initialize core components
        App.initAjax();

        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;

    });
    
    $scope.redirectToRoleEdit = function (id) {
        $state.go("permission", { id: id });
    };

    $scope.add = function () {
        $state.go("permission");
    }

    function generateButtonsColumn(dataItem) {
        return '<a href="" class="k-button" data-ng-click="redirectToRoleEdit(' + dataItem.id + ')">Edit</a>';
    };

    $scope.options = {
        height: 650,
        columns: [{ field: "Name", title: "Name", width: "500px" },
        { template: generateButtonsColumn, width: 100 }],
        dataSource: {
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
            pageSize: 10,
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "Id",
                    fields: {
                        //Id: {type:"number"},
                        Name: { editable: false, type: "string" },
                    }
                }
            },
            error: function (e) {
                var msg = e.xhr.responseText;
                $scope.savedSuccessfully = false;
                $scope.message = msg;
            },
            transport: {
                read: function (options) {
                    
                    roleService.getList(options.data).then(function (data) {
                        options.success(data.data);
                    });

                },
                parameterMap: function (data, operation) {
                    return JSON.stringify(data);
                },
            }
        },

        pageable: true,
        sortable: true,
        filterable: true,

    };


}]);
