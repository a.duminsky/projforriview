﻿/* Firm Setup page controller */
angular.module('DexteraApp').controller('CaseListController', ['$rootScope', '$scope', '$state', 'settings', 'caseService', function ($rootScope, $scope, $state, settings, firmService) {
    $scope.$on('$viewContentLoaded', function() {   
        // initialize core components
        App.initAjax();

        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;
    });

    function generateButtonsColumn(dataItem) {
        return '<a href="" class="k-button" data-ng-click="edit('+dataItem.id+')">Edit</a>';
    };

    $scope.add = function () {
        $state.go('case');
    }

    $scope.edit = function (id) {
        $state.go('case', { id: id });
    }

    $scope.options = {
        height: 650,
        columns: [{ field: "AddressStreet", title: "Address Street", width: "100px" },
            { field: "AddressUnit", title: "Address Unit", width: "100px" },
            { field: "AddressCity", title: "Address City", width: "100px" },
            { field: "AddressState", title: "Address State", width: "100px" },
            { field: "AddressZip", title: "Address Zip", width: "100px" },
            { field: "AddressPhone", title: "Address Country", width: "100px" },
            
            { template: generateButtonsColumn, width: 100 }],
        dataSource: {
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
            pageSize: 10,
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "Id",
                    fields: {
                        Id: {type:"number"},
                        AddressStreet: { editable: false, type: "string" },
                        AddressUnit: { editable: false, type: "string" },
                        AddressCity: { editable: false, type: "string" },
                        AddressState: { editable: false, type: "string" },
                        AddressZip: { editable: false, type: "string" },
                        AddressPhone: { editable: false, type: "string" },
                     
                    }
                }
            },
            error: function (e) {
                var msg = e.xhr.responseText;
                $scope.savedSuccessfully = false;
                $scope.message = msg;
            },
            transport: {
                read: function (options) {
                    firmService.getList(options.data).then(function (result) {
                        options.success(result.data);
                    })
                    
                },
                parameterMap: function (data, operation) {
                    return JSON.stringify(data);
                },
            }
        },
        
        pageable: true,
        sortable: true,
        filterable: true,
        
    };

}]);
