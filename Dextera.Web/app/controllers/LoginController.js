﻿/* Firm Setup page controller */
angular.module('DexteraApp').controller('LoginController', ['$rootScope', '$scope', 'settings', '$location', 'accountService', function ($rootScope, $scope, settings, $location, accountService) {
    $scope.$on('$viewContentLoaded', function() {   
        // initialize core components
        App.initAjax();

        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;

    });

    $scope.loginData = {};

    $scope.login = function () {
        $scope.error = "";
        accountService.login($scope.loginData.login, $scope.loginData.password).then(
            function (response) {
                localStorage.authData = response.data.access_token;
                $location.path('/dashboard');
            });
    }
    
}]);
