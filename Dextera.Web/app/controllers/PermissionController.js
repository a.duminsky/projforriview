﻿/* Firm Setup page controller */
angular.module('DexteraApp').controller('PermissionController', ['$rootScope', '$scope', '$stateParams', 'settings', 'roleService', 'accountService', '$state',
    'notificationService',
    function ($rootScope, $scope, $stateParams, settings, roleService, accountService, $state, notificationService) {
    $scope.$on('$viewContentLoaded', function () {
        // initialize core components
        App.initAjax();

        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;

    });
    
        roleService.get($stateParams.id ? $stateParams.id : 0).then(function (data) {
            $scope.dataRole = data.data
        });
    
        $scope.AccessLevels = [
            { value: 1, text: "Yes" },
            { value: 2, text: "Some / Special" },
            { value: 3, text: "Some" },
            { value: 4, text: "Yes / Some" },
            { value: 5, text: "No" },
            { value: 6, text: "Special" },
        ]
    $scope.roleServiceUpdate = function (data) {
        if ($stateParams.id) {
            roleService.update(data).then(function () {
                notificationService.showSuccess("Role saved sucessfully!");
                $state.go("role");
            }, $scope.showValidation);
        }
        else
        {
            roleService.add(data).then(function () {
                notificationService.showSuccess("Role saved sucessfully!");
                $state.go("role");
            });
        }
    };
}]);
