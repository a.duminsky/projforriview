/* Setup ManageUsers page controller */
angular.module('DexteraApp').controller('ManageUsersController', ['$rootScope', '$scope', '$state', 'settings', 'accountService', function ($rootScope, $scope, $state, settings, accountService) {
    $scope.$on('$viewContentLoaded', function () {
        // initialize core components
        App.initAjax();

        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;

    });
    function generateButtonsColumn(dataItem) {
        return '<a href="" class="k-button" data-ng-click="edit(\''+dataItem.id+'\')">Edit</a>';
    };

    $scope.add = function () {
        $state.go('user');
    }

    $scope.edit = function (id) {
        $state.go('user', { id: id });
    }

  

    $scope.optionsUser = {
        height: 650,
        columns: [{ field: "FirstName", title: "First Name", width: "100px" },
            { field: "LastName", title: "Last Name", width: "100px" },
        { field: "Email", title: "Email", width: "100px" },
        { field: "Phone", title: "Phone Number", width: "100px" },
        { field: "SNN", title: "SNN", width: "100px" },
        { template: generateButtonsColumn, width: 100 }],
        dataSource: {
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
            pageSize: 10,
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "Id",
                    fields: {
                        SNN: { editable: false, type: "string" },
                        FirstName: { editable: false, type: "string" },
                        LastName: { editable: false, type: "string" },
                        Email: { editable: false, type: "string" },
                        PhoneNumber: { editable: false, type: "string" },
                    }
                }
            },
            error: function (e) {
                var msg = e.xhr.responseText;
                $scope.savedSuccessfully = false;
                $scope.message = msg;
            },
            transport: {
                read: function (options) {
                    accountService.getList(options.data).then(function (result) {
                        options.success(result.data);
                    })

                },
                parameterMap: function (data, operation) {
                    return JSON.stringify(data);
                },
            }
        },

        pageable: true,
        sortable: true,
        filterable: true,
        //editable: "inline",
    };
}]);

 