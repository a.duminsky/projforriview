/* Setup blank page controller */
angular.module('DexteraApp').controller('UserController', ['$rootScope', '$scope', '$state', '$stateParams', 'roleService', 'settings', 'accountService', 'groupService',
    'notificationService', 'selectService',
    function ($rootScope, $scope, $state, $stateParams, roleService, settings, accountService, groupService,
        notificationService, selectService) {
    $scope.$on('$viewContentLoaded', function() {   
        // initialize core components
        App.initAjax();

        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;
    });
    
    $scope.get = function () {
        if ($stateParams.id) {
            accountService.get($stateParams.id).then(function (data) {
                $scope.dataUser = data.data
            });
        }
        else {
            $scope.dataUser = {};
        }
    }

    $scope.cancel = function ()
    {
        $state.go("manageUsers")
    }

    $scope.UserServiceUpdate = function () {
        if ($stateParams.id) {
            accountService.update($scope.dataUser).then(function () {
                notificationService.showSuccess("User saved sucessfully!");
                $state.go("manageUsers")
            }, $scope.showValidation);
        }
        else {
            if ($scope.dataUser.Password === null || $scope.dataUser.Password.length < 8) {
                notificationService.showError("Password length should not less than 8");
            }
            else if ($scope.dataUser.Password !== $scope.dataUser.PasswordConfirm) {
                notificationService.showError("Password and Password confirm should be same");
            }
            else
            {
                accountService.add($scope.dataUser).then(function () {
                    $state.go("manageUsers")
                }, $scope.showValidation);
            }
        }
    }

    $scope.groupsOptions = selectService.getMultySelectOptions('group', 'groups')
    $scope.get(); 

    $scope.rolesOptions = {
        placeholder: "Select roles...",
        dataTextField: "Name",
        dataValueField: "Id",
        valuePrimitive: true,
        autoBind: false,
        dataSource: {
            type: "odata",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    roleService.getList(options.data).then(function (result) {
                        options.success({
                            "d": {
                                "results": result.data.Data, "__count": result.data.Count
                            }
                        })
                    })

                },
                parameterMap: function (data, operation) {
                    return JSON.stringify(data);
                },
            }
        }
    };
    $scope.get(); 
}]);
