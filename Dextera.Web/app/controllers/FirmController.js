﻿/* Firm Setup page controller */
angular.module('DexteraApp').controller('FirmController', ['$rootScope', '$scope', '$state', '$stateParams', 'settings', 'firmService', 'notificationService',
    function ($rootScope, $scope, $state, $stateParams, settings, firmService, notificationService) {
    $scope.$on('$viewContentLoaded', function() {   
        // initialize core components
        App.initAjax();

        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;

    });
    
    $scope.get = function () {
        if ($stateParams.id) {
            firmService.get($stateParams.id).then(function (result) {
                $scope.firm = result.data;
            })
        }
    }

    $scope.save = function () {
        if ($stateParams.id)
        {
            firmService.update($scope.firm).then(function () {
                notificationService.showSuccess("Firm saved sucessfully!");
                $state.go("firms")
            }, $scope.showValidation);
        }
        else {
            firmService.add($scope.firm).then(function () {
                notificationService.showSuccess("Firm saved sucessfully!");
                $state.go("firms")
            }, $scope.showValidation);
        }
    }
    $scope.get();
    

}]);
