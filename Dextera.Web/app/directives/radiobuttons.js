

angular.module('DexteraApp').directive('radioButtons', ['$http', function ($http) {

    return {
        restrict: 'E',
        template: '<span ng-repeat="option in data track by option.Id"><input type="radio" name="{{type}}" required ng-model="model" value="{{option.Id}}">' +
        '{{option.Name }}</span >',
        replace: true,
        scope: true,
        type: "=",
        
        link: function (scope, element, attrs) {
           
            element.click(function () {
                console.log("clicked....");
                // body...
            });

            $http.get(serviceBase + 'api/common/' + attrs.type + "?" + kendo.stringify({})).then(function (result) {
                scope.type = attrs.type;
                scope.data = result.data.Data
            })
            console.log("link should be work as per our expections...", + attrs.valve);

            scope.$watch("model", function (newValue, oldValue) {
                var sc = scope.$parent.$parent.$parent;
                eval("sc." + attrs.ngModel + " = " + newValue);
            });

            scope.$parent.$parent.$parent.$watch(attrs.ngModel, function (newValue, oldValue) {
                scope.model = newValue;
                console.log("ngModel");
            });
            

        }
    }
}]);