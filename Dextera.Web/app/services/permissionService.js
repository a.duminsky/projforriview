﻿'use strict';
angular.module('DexteraApp').factory('permissionService', ['$http', 'ngAuthSettings', function ($http, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var factory = {};

    var _getList = function () {
        return $http.get(serviceBase + 'api/permission');
    };

    var _get = function (id) {
        return $http.get(serviceBase + 'api/permission/'+id);
    };

    var _update = function (entity) {
        return $http.post(serviceBase + 'api/permission/', entity);
    };

    var _add = function (entity) {
        return $http.put(serviceBase + 'api/permission/', entity);
    };

    var _delete = function (entity) {
        return $http.delete(serviceBase + 'api/permission/', entity);
    };
    factory.getList = _getList;
    factory.get = _get;
    factory.update = _update;
    factory.add = _add;
    factory.delete = _delete;
    

    
    return factory;

}]);