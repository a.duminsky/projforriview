﻿'use strict';
angular.module('DexteraApp').factory('clientService', ['$http', 'ngAuthSettings', function ($http, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var factory = {};

    var _getList = function (data) {
        return $http.get(serviceBase + 'api/client?' + kendo.stringify(data));
    };

    var _get = function (id) {
        return $http.get(serviceBase + 'api/client/'+id);
    };

    var _update = function (entity) {
        return $http.post(serviceBase + 'api/client/', entity);
    };

    var _add = function (entity) {
        return $http.put(serviceBase + 'api/client/', entity);
    };

    var _delete = function (entity) {
        return $http.delete(serviceBase + 'api/client/', entity);
    };
    factory.getList = _getList;
    factory.get = _get;
    factory.update = _update;
    factory.add = _add;
    factory.delete = _delete;
    

    
    return factory;

}]);