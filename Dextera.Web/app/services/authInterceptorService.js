﻿'use strict';
angular.module('DexteraApp').factory('authInterceptorService', ['$rootScope', '$q', '$injector', '$location', 'notificationService',
    function ($rootScope, $q, $injector, $location, notificationService) {

    var authInterceptorServiceFactory = {};

    var _request = function (config) {
        $rootScope.$broadcast('$stateChangeStart')
        config.headers = config.headers || {};

        var authData = localStorage.authData;
        if (authData) {
            config.headers.Authorization = 'Bearer ' + authData;
        }
        //if (config.url.indexOf("/api/") > 0) {
        //    config.headers['Access-Control-Allow-Origin'] = '*';
        //    config.headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS, PUT, DELETE';
        //    config.headers['Content-Type'] = 'application/json';
        //    config.headers['Accept'] = 'application/json';
        
        //}
        return config;
    }

        
    var _responseError = function (rejection) {
        if (rejection.data && !rejection.data.ModelState) {
            if (rejection.data.Message) {
                notificationService.showError(rejection.data.Message);
            }
            else if (rejection.data.error_description) {
                notificationService.showError(rejection.data.error_description);
            }
        }
        if (rejection.data && rejection.data.ModelState) {
            rejection.data.ValidationErrors = {};
            for (var k in rejection.data.ModelState) {

                var field = k;
                if (k.indexOf(".") > -1) {
                    field = k.substr(k.indexOf(".") + 1);
                }
                rejection.data.ValidationErrors[field] = rejection.data.ModelState[k][0];
            }
        }
        if (rejection.status === 401) {
            $location.path('/login');
            /*var authService = $injector.get('authService');
            var authData = localStorageService.get('authorizationData');

            if (authData) {
                if (authData.useRefreshTokens) {
                    $location.path('/refresh');
                    return $q.reject(rejection);
                }
            }
            
            authService.logOut();*/
            //window.location.href = "#/login";
            //window.location = '/login';
        }
        $rootScope.$broadcast('$stateChangeSuccess')
        return $q.reject(rejection);
    }

    

    var _response =  function(response) {
        // do something on success
        $rootScope.$broadcast('$stateChangeSuccess')
        return response;
    }

        
    authInterceptorServiceFactory.response = _response;
    authInterceptorServiceFactory.request = _request;
    authInterceptorServiceFactory.responseError = _responseError;

    return authInterceptorServiceFactory;
}]);