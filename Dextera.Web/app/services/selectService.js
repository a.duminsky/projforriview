﻿'use strict';
angular.module('DexteraApp').factory('selectService', ['$http', 'ngAuthSettings', function ($http, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var factory = {};

    var _getMultySelectOptions = function (type, name) {
        return {
            placeholder: "Select " +name+ "...",
            dataTextField: "Name",
            dataValueField: "Id",
            valuePrimitive: true,
            autoBind: false,
            tagMode: "single",
            index: 1,
            dataSource: {
                type: "odata",
                serverFiltering: true,
                transport: {
                    read: function (options) {
                        $http.get(serviceBase + 'api/common/' + type + "?" + kendo.stringify(options.data)).then(function (result) {
                            options.success({
                                "d": {
                                    "results": result.data.Data, "__count": result.data.Count
                                }
                            })
                        })
                    },

                    parameterMap: function (data, operation) {
                        return JSON.stringify(data);
                    },
                    
                    dataBound: function (e) {
                        e.sender.select(e.sender.dataItem(0));
                    }
                }
            }
        };
    };

    var _getSelectOptions = function (type, name) {
        return $http.get(serviceBase + 'api/common/' + type + "?" + kendo.stringify({})).then(function (result) {
           return result.data.Data;
        })
        
    };


   

    

    factory.getSelectOptions = _getSelectOptions;
    factory.getMultySelectOptions = _getMultySelectOptions;
    return factory;

}]);