﻿'use strict';
angular.module('DexteraApp').factory('accountService', ['$http', 'ngAuthSettings', function ($http, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var factory = {};

    var _getList = function (data) {
        return $http.get(serviceBase + 'api/account?' + kendo.stringify(data));
    };

    var _get = function (id) {
        return $http.get(serviceBase + 'api/account/'+id);
    };

    var _update = function (entity) {
        return $http.post(serviceBase + 'api/account/', entity);
    };

    var _add = function (entity) {
        return $http.put(serviceBase + 'api/account/', entity);
    };

    var _delete = function (entity) {
        return $http.delete(serviceBase + 'api/account/', entity);
    };


    var _login = function (login, pass) {
        var loginData = 'grant_type=password&username=' + login + '&password=' + pass;
        return $http.post(serviceBase + "token", loginData, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } })
    }

    factory.getList = _getList;
    factory.get = _get;
    factory.update = _update;
    factory.add = _add;
    factory.delete = _delete;
    factory.login = _login;

    
    return factory;

}]);