﻿'use strict';
angular.module('DexteraApp').factory('defenantService', ['$http', 'ngAuthSettings', function ($http, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var factory = {};

    var _getList = function (data, caseId) {
        return $http.get(serviceBase + 'api/defenant/'+caseId+'?' + kendo.stringify(data));
    };

    var _get = function (id) {
        return $http.get(serviceBase + 'api/defenant/'+id);
    };

    var _update = function (entity) {
        return $http.post(serviceBase + 'api/defenant/', entity);
    };

    var _add = function (entity) {
        return $http.put(serviceBase + 'api/defenant/', entity);
    };

    var _delete = function (entity) {
        return $http.delete(serviceBase + 'api/defenant/', entity);
    };
    factory.getList = _getList;
    factory.get = _get;
    factory.update = _update;
    factory.add = _add;
    factory.delete = _delete;
    

    
    return factory;

}]);