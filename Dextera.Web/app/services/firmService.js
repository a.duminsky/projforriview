﻿'use strict';
angular.module('DexteraApp').factory('firmService', ['$http', 'ngAuthSettings', function ($http, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var factory = {};

    var _getList = function (data) {
        return $http.get(serviceBase + 'api/firm?' + kendo.stringify(data));
    };

    var _get = function (id) {
        return $http.get(serviceBase + 'api/firm/'+id);
    };

    var _update = function (entity) {
        return $http.post(serviceBase + 'api/firm/', entity);
    };

    var _add = function (entity) {
        return $http.put(serviceBase + 'api/firm/', entity);
    };

    var _delete = function (entity) {
        return $http.delete(serviceBase + 'api/firm/', entity);
    };
    factory.getList = _getList;
    factory.get = _get;
    factory.update = _update;
    factory.add = _add;
    factory.delete = _delete;
    

    
    return factory;

}]);