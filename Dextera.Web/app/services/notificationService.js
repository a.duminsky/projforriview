﻿'use strict';
angular.module('DexteraApp').factory('notificationService', ['$rootScope', function($rootScope) {
    var factory = {};

    var _show = function (text, type) {
        $rootScope.$broadcast('showNotification', text, type);
    };

    var _showError = function (text) {
        _show(text, "error");
    }

    var _showInfo = function (text) {
        _show(text, "info");
    }

    var _showWarning = function (text) {
        _show(text, "warning");
    }

    var _showSuccess = function (text) {
        _show(text, "success");
    }

    factory.show = _show;
    factory.showError = _showError;
    factory.showInfo = _showInfo;
    factory.showWarning = _showWarning;
    factory.showSuccess = _showSuccess;
    
    return factory;

}]);