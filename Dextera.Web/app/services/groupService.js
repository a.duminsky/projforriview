﻿'use strict';
angular.module('DexteraApp').factory('groupService', ['$http', 'ngAuthSettings', function ($http, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var factory = {};

    var _getList = function (data) {
        return $http.get(serviceBase + 'api/group?' + kendo.stringify(data));
    };
   

    var _get = function (id) {
        return $http.get(serviceBase + 'api/group/'+id);
    };

    var _update = function (entity) {
        return $http.post(serviceBase + 'api/group/', entity);
    };

    var _add = function (entity) {
        return $http.put(serviceBase + 'api/group/', entity);
    };

    var _delete = function (entity) {
        return $http.delete(serviceBase + 'api/group/', entity);
    };
    factory.getList = _getList;
    factory.get = _get;
    factory.update = _update;
    factory.add = _add;
    factory.delete = _delete;
    

    
    return factory;

}]);